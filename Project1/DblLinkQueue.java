public class DblLinkQueue<Item> implements Queue<Item> {
   protected static class Node {
      public Object data;
      public Node next, prev;

      public Node(Object d, Node p, Node n) {
         data = d;
         next = n;
         prev = p;
      }
   }

   protected Node mHead = new Node(null, null, null);

   public void add(Item val) {
      Node nd;

      for (nd = mHead; nd.next != null; nd = nd.next)
         ;

      nd.next = new Node(val, nd, null);
   }

   public Item getFront() {return isEmpty() ? null : (Item)mHead.next.data;}

   public void remove() {
      mHead.next = mHead.next.next;

      if (mHead.next != null)
         mHead.next.prev = mHead;
   }

   public boolean isEmpty() {return mHead.next == null;}

   public boolean equals(Object obj) {
      Node nd1, nd2;
      DblLinkQueue o;

      if (obj == null || !(obj instanceof DblLinkQueue))
         return false;

      o = (DblLinkQueue)obj;

      if (isEmpty() || o.isEmpty())
         return isEmpty() && o.isEmpty();

      for (nd1 = mHead.next, nd2 = o.mHead.next;
       nd1 != null && nd2 != null; nd1 = nd1.next, nd2 = nd2.next)
         if (!nd1.data.equals(nd2.data))
            return false;
     
      return nd1 == null && nd2 == null;
   }

   public Object clone() {
      DblLinkQueue copy = null;
      Node nd1, nd2;

      try {
         copy = (DblLinkQueue)super.clone();

         copy.mHead = new Node(null, null, null);

         for (nd1 = mHead.next, nd2 = copy.mHead; nd1 != null;
          nd1 = nd1.next, nd2 = nd2.next)
            nd2.next = new Node(nd1.data, nd2, null);
      }
      catch (CloneNotSupportedException err) {}

      return copy;
   }
}

