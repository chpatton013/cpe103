import java.util.*;

public class ArrPQueue<Item> extends ArrQueue<Item> {
   protected Comparator<Item> mCmp;
   
   public ArrPQueue(Comparator<Item> cmp) {mCmp = cmp;}
      
   public void add(Item val) {
      int ndx;

      super.add(val);

      for (ndx = (mTail - 1 + mData.length) % mData.length;
       ndx != (mHead - 1 + mData.length) % mData.length &&
       (mTail - mHead + mData.length) % mData.length != 1;
       ndx = (ndx - 1 + mData.length) % mData.length) {
         if (mCmp.compare((Item)mData[ndx], val) < 0) {
            mData[(ndx + 1 + mData.length) % mData.length] = mData[ndx];
            mData[ndx] = val;
         }
      }
   }
}

