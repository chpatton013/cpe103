import java.util.*;

public class LinkPQueue<Item> extends LinkQueue<Item> {
   protected Comparator<Item> mCmp;
   
   public LinkPQueue(Comparator<Item> cmp) {mCmp = cmp;}
   
   public void add(Item val) {
      Node nd;

      if (mHead == null) {
         mHead = new Node(val, null);
         mTail = mHead;
         return;
      }

      if (mHead.next == null) {
         if (mCmp.compare((Item)mHead.data, val) >= 0) {
            mHead.next = new Node(val, null);
            mTail = mHead.next;
         }
         else
            mHead = new Node(val, mHead);

         return;
      }

      if (mCmp.compare((Item)mHead.data, val) < 0) {
         mHead = new Node(val, mHead);
         return;
      }

      for (nd = mHead; nd.next != null; nd = nd.next) {
         if (mCmp.compare((Item)nd.next.data, val) < 0) {
            nd.next = new Node(val, nd.next);
            return;
         }
      }

      if (nd.next == null)
         nd.next = mTail = new Node(val, null);
   }
}

