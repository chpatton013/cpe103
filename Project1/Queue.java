public interface Queue<Item> extends Cloneable {
   public abstract void add(Item val);
   public abstract Item getFront();
   public abstract void remove();
   public abstract boolean isEmpty();
   public abstract Object clone();
}