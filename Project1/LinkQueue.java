public class LinkQueue<Item> implements Queue<Item> {
   protected static class Node {
      public Object data;
      public Node next;
      
      public Node(Object d, Node n) {data = d; next = n;}
   }
   
   protected Node mHead, mTail;
   
   public Item getFront() {return (mHead == null) ? null : (Item)mHead.data;}

   public boolean isEmpty() {return mHead == null;}

   public void add(Item val) {
      if (mHead == null) {
         mHead = new Node(val, null);
         mTail = mHead;
      }
      else {
         mTail.next = new Node(val, null);
         mTail = mTail.next;
      }
   }

   public void remove() {
      mHead = mHead.next;
      mTail = mHead == null ? null : mTail;
   }

   public boolean equals(Object obj) {
      Node nd1, nd2;
      LinkQueue o;

      if (obj == null || !(obj instanceof LinkQueue))
         return false;

      o = (LinkQueue)obj;

      for (nd1 = mHead, nd2 = o.mHead; nd1 != null && nd2 != null;
       nd1 = nd1.next, nd2 = nd2.next) {
         if (!nd1.data.equals(nd2.data))
            return false;
      }

      return nd1 == nd2;
   }

   public Object clone() {
      LinkQueue copy = null;
      Node nd1, nd2;

      try {
         copy = (LinkQueue)super.clone();

         if (mHead != null) {
            copy.mHead = new Node(mHead.data, mHead.next);

            for (nd1 = mHead.next, nd2 = copy.mHead; nd1 != null;
             nd1 = nd1.next, nd2 = nd2.next)
               nd2.next = new Node(nd1.data, nd1.next);

            copy.mTail = nd2;
         }
      }
      catch (CloneNotSupportedException err) {}

      return copy;
   }
}

