//package com.cstaley.algebra;

//import com.cstaley.algebra.Queue;

import java.util.Comparator;
import java.util.Scanner;

public class BinaryHeap<Item> implements Queue {
   private final static int dfltSize = 63;

   int mTail = 0;
   Object[] mArr = new Object[dfltSize];
   Comparator<Item> cmp;

   public BinaryHeap(Comparator cmp) {this.cmp = cmp;}

   public void add(Object val) {
      Object[] temp;

      if (mTail == mArr.length) {
         temp = new Object[nextLevel(mTail)];
         System.arraycopy(mArr, 0, temp, 0, temp.length);
         mArr = temp;
      }

      mArr[mTail] = val;
      promote(mTail++);
   }

   public Item getFront() {return isEmpty() ? null : (Item)mArr[0];}

   public void remove() {
      if (mTail > 1) {
         mArr[0] = mArr[--mTail];
         demote(0);
      }
      else if (mTail > 0)
         --mTail;
   }

   public boolean isEmpty() {return mTail == 0;}

   public Object clone() {
      int ndx;
      BinaryHeap copy = null;

      try {
         copy = (BinaryHeap)super.clone();

         copy.mArr = new Object[nextLevel(mTail)];

         System.arraycopy(mArr, 0, copy.mArr, 0, size());
      }
      catch (CloneNotSupportedException err1) {}

      return copy;
   }

   public boolean equals(Object obj) {
      int ndx1, ndx2;
      BinaryHeap o;

      if (obj == null || !(obj instanceof BinaryHeap))
         return false;

      o = (BinaryHeap)obj;

      if (mTail != o.mTail)
         return false;

      for (ndx1 = 0, ndx2 = 0; ndx1 < mTail; ndx1++, ndx2++)
         if (!mArr[ndx1].equals(o.mArr[ndx2]))
            return false;

      return true;
   }
   
   public String toString() {
      int ndx;
      String rtn = "[";

      for (ndx = 0; ndx < mTail; ++ndx) {
         rtn += ndx != mTail - 1 ? mArr[ndx].toString() + ", " :
          mArr[ndx].toString();
      }

      return rtn + "]";
   }

   private int size() {return mTail;};

   private static int childIndex(int ndx) {return 2 * ndx + 1;}

   private static int parentIndex(int ndx) {return (ndx - 1) / 2;}

   private static int indexLevel(int ndx) {
      return (int)Math.ceil(log(2, (ndx + 1) / 2));
   }

   private static int nextLevel(int ndx) {
      return (int)Math.pow(2, indexLevel(ndx) + 1) - 1;
   }

   private static double log(double base, double val) {
      return Math.log(val) / Math.log(base);
   }

   private void promote(int ndx) {
      Object temp;

      if (ndx == 0 || cmp.compare((Item)mArr[ndx],
       (Item)mArr[parentIndex(ndx)]) <= 0)
         return;

      temp = mArr[ndx];
      mArr[ndx] = mArr[parentIndex(ndx)];
      mArr[parentIndex(ndx)] = temp;

      promote(parentIndex(ndx));
   }

   private void demote(int ndx) {
      int child;
      Object temp;

      if (ndx == size() - 1)
         return;

      child = childIndex(ndx);

      if (cmp.compare((Item)mArr[ndx], (Item)mArr[child]) > 0 &&
       cmp.compare((Item)mArr[ndx], (Item)mArr[child + 1]) > 0)
         return;

      if (cmp.compare((Item)mArr[child], (Item)mArr[child + 1]) < 0)
         ++child;

      temp = mArr[ndx];
      mArr[ndx] = mArr[child];
      mArr[child] = temp;

      demote(child);
   }
}
