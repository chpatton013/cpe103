public class ArrQueue<Item> implements Queue<Item> {
   protected static final int mIncrSize = 10;
   protected Object[] mData = new Object[mIncrSize];
   protected int mHead, mTail;
   protected boolean mEmpty = true;
   
   public Item getFront() {return (Item)mData[mHead];}
   
   public boolean isEmpty() {return mEmpty;}

   public void add(Item val) {
      Object[] temp;

      if (mHead != mTail || mEmpty) {
         mData[mTail] = val;
         mTail = (mTail + 1 + mData.length) % mData.length;
      }
      else {
         temp = new Object[mData.length + mIncrSize];

         System.arraycopy(mData, mHead, temp, 0, mData.length - mHead);
         System.arraycopy(mData, 0, temp, mData.length - mHead, mTail);

         temp[mData.length] = val;

         mHead = 0;
         mTail = mData.length + 1;
         mData = temp;
      }

      mEmpty = false;
   }
   
   public void remove() {
      mHead = (mHead + 1) % mData.length;
      mEmpty = mHead == mTail;
   }
   
   public boolean equals(Object obj) {
      int mSz, oSz, ndx, mNdx, oNdx;
      ArrQueue o;

      if (obj == null || !(obj instanceof ArrQueue))
         return false;

      o = (ArrQueue)obj;

      if (this.mEmpty != o.mEmpty)
         return false;

      mSz = mEmpty ? 0 : (mTail - mHead - 1 + mData.length) % mData.length + 1;
      oSz = o.mEmpty ? 0 : (o.mTail - o.mHead - 1 + o.mData.length) %
       o.mData.length + 1;

      if (mSz != oSz)
         return false;

      for (ndx = 0; ndx < mSz; ndx++) {
         mNdx = (mHead + ndx + mData.length) % mData.length;
         oNdx = (o.mHead + ndx + o.mData.length) % o.mData.length;

         if (!mData[mNdx].equals(o.mData[oNdx]))
            return false;
      }

      return true;
   }
   
   public Object clone() {
      int size = (mTail - mHead + mData.length) % mData.length;
      ArrQueue copy = null;

      try {
         copy = (ArrQueue)super.clone();
         copy.mData = new Object[mData.length];
         System.arraycopy(mData, 0, copy.mData, 0, mData.length);
      }
      catch (CloneNotSupportedException err) {}

      return copy;
   }
}

