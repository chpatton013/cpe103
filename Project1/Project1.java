import java.util.Scanner;
import java.util.Comparator;

public class Project1 {
   private Queue[] q;
   private Scanner sc = new Scanner(System.in);

   static public void main(String[] args) {
      Project1 app = new Project1();
      app.run(args);
   }

   static class DoubleComparator implements Comparator<Double> {
      final static double epsilon = 0.00001;

      public int compare(Double d1, Double d2) {
         double diff;

         if (d1 == null && d2 == null)
            return 0;
         else if (d1 == null)
            return -1;
         else if (d2 == null)
            return 1;

         diff = d1 - d2;

         if (diff > epsilon)
            return 1;
         else if (diff < -epsilon)
            return -1;
         else
            return 0;
      }
   }

   private void run(String[] args) {
      int ndx, numQ;
      String qType, cmd;
      Class qClass;
      Class[] paramTypes = {Comparator.class};
      Object[] params = {new DoubleComparator()};

      if (args.length != 1) {
         System.out.println("Usage: Project1 queueType");
         return;
      }

      qType = args[0];
      try {
         qClass = (Class<Queue<Double>>)Class.forName(qType);
      }
      catch (Exception err) {
         System.out.println("Error building queue: " + err);
         return;
      }

      System.out.print("Enter queue count: ");
      numQ = sc.nextInt();
      q = new Queue[numQ];

      for (ndx = 0; ndx < numQ; ndx++) {
         try {
            q[ndx] = (Queue)qClass.newInstance();
         }
         catch (Exception err1) {
            try {
               q[ndx] = (Queue)qClass.getConstructor(paramTypes).
                newInstance(params);
            }
            catch (Exception err2) {
               err2.printStackTrace();
               return;
            }
         }
      }

      while (true) {
         System.out.print("Enter command: ");
         cmd = sc.next();

         if (cmd.equals("A") || cmd.equals("a"))
            add();
         else if(cmd.equals("F") || cmd.equals("f"))
            front();
         else if(cmd.equals("R") || cmd.equals("r"))
            remove();
         else if(cmd.equals("E") || cmd.equals("e"))
            empty();
         else if(cmd.equals("="))
            equal();
         else if(cmd.equals("C") || cmd.equals("c"))
            copy();
         else if(cmd.equals("S") || cmd.equals("s"))
            stability();
         else if(cmd.equals("M") || cmd.equals("m"))
            memory();
         else if(cmd.equals("Q") || cmd.equals("q")) {
            quit();
            return;
         }
         else
            System.out.println("Invalid command");
      }
   }

   private void add() {
      int ndx, qNum = sc.nextInt(), numItems = sc.nextInt();

      if (qNum < 0 || qNum >= q.length)
         System.out.println("Invalid command");

      for (ndx = 0; ndx < numItems; ndx++)
         q[qNum].add(sc.nextDouble());
   }

   private void front() {
      int qNum = sc.nextInt();

      if (qNum < 0 || qNum >= q.length)
         System.out.println("Invalid command");
      else
         System.out.println("Queue " + qNum + " has front: " +
          q[qNum].getFront());
   }

   private void remove() {
      int ndx, qNum = sc.nextInt(), numItems = sc.nextInt();

      if (qNum < 0 || qNum >= q.length)
         System.out.println("Invalid command");
      else
         for (ndx = 0; ndx < numItems; ndx++) {
            if (q[qNum].isEmpty())
               return;
            else
               q[qNum].remove();
         }
   }

   private void empty() {
      int qNum = sc.nextInt();

      if (qNum < 0 || qNum >= q.length)
         System.out.println("Invalid command");
      else if (q[qNum].isEmpty())
         System.out.println("Queue " + qNum + " is empty");
      else
         System.out.println("Queue " + qNum + " is not empty");
   }

   private void equal() {
      int qNum1 = sc.nextInt(), qNum2 = sc.nextInt();

      if (qNum1 < 0 || qNum1 >= q.length || qNum2 < 0 || qNum2 >= q.length)
         System.out.println("Invalid command.");
      else if (q[qNum1].equals(q[qNum2]))
         System.out.println("Queue " + qNum1 + " == Queue " + qNum2);
      else
         System.out.println("Queue " + qNum1 + " != Queue " + qNum2);
   }

   private void copy() {
      int qNum1 = sc.nextInt(), qNum2 = sc.nextInt();

      if (qNum1 < 0 || qNum1 >= q.length || qNum2 < 0 || qNum2 >= q.length)
         System.out.println("Invalid command");
      else
         q[qNum1] = (Queue)q[qNum2].clone();
   }

   private void stability() {
      int ndx, qNum = sc.nextInt(), count = sc.nextInt();
      Double[] data = new Double[count];
      boolean pass = true;

      if (qNum < 0 || qNum >= q.length) {
         System.out.println("Invalid command");
         return;
      }

      for (ndx = 0; ndx < count; ndx++) {
         data[ndx] = new Double(10.0);
         q[qNum].add(data[ndx]);
      }

      while (q[qNum].getFront() != data[0])
         q[qNum].remove();

      for (ndx = 0; ndx < count; ndx++) {
         pass = (q[qNum].getFront() == data[ndx]);
         q[qNum].remove();
      }

      if (!pass)
         System.out.println("Stability test failed");
   }

   private void memory() {
      int ndx, qNum = sc.nextInt(), count = sc.nextInt();

      if (qNum < 0 || qNum >= q.length) {
         System.out.println("Invalid command");
         return;
      }

      for (ndx = 0; ndx < count; ndx++) {
         q[qNum].add(10.0);
         q[qNum].remove();
      }
   }

   private void quit() {System.out.println("Exiting...");}
}

