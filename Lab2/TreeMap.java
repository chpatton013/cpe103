package com.cstaley.cpe103.BST;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Comparator;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

public class TreeMap<K, V> implements Map<K, V> {
   static public class Node {
      Object key;
      Object data;
      Node left;
      Node right;

      public Node(Object key, Object data, Node left, Node right) {
         this.key = key;
         this.data = data;
         this.left = left;
         this.right = right;
      }
   }
   
   private static class FindResult {
      public Node nd;
      public Node parent;
      public boolean onLeft;
   }
   
   public class TreeEntry implements Map.Entry<K, V> {
      private K key;
      private V value;

      public TreeEntry(Object k, Object v) {
         key = (K)k;
         value = (V)v;
      }

      public K getKey() {return key;}

      public V getValue() {return value;}

      public V setValue(V value) {
         throw new UnsupportedOperationException();
      }

      public String toString() {
         return "(" + key + ", " + value + ")";
      }

      public boolean equals(Object obj) {
         TreeEntry o;

         if (obj == null || !(obj.getClass().equals(TreeEntry.class)))
            return false;

         o = (TreeEntry)obj;

         return key == o.key && value == o.value;
      }
   }

   private Comparator<? super K> cmp;
   private Node root;
   private int size;
   private final String indentStr = "   ";
   
   public TreeMap() {
      cmp = new Comparator<K>() {
         public int compare(Object k1, Object k2) {
            return ((Comparable)k1).compareTo((Comparable)k2);
         }
      };
   }
   
   public TreeMap(Comparator<? super K> cmp) {
      this.cmp = cmp;
   }

   @Override
   public void clear() {
      root = null;
      size = 0;
   }

   @Override
   public boolean isEmpty() {
      return root == null;
   }
   
   @Override
   public int size() {return size;}
   
   @Override
   public boolean containsKey(Object key) {
      return findNode(root, key).nd != null;
   }

   private FindResult findNode(Node rt, Object key) {
      int cmpVal;
      FindResult rtn = new FindResult();
      
      for (rtn.nd = rt; rtn.nd != null;) {
         cmpVal = cmp.compare((K)key, (K)rtn.nd.key);
         if (cmpVal == 0)
            break;
         else {
            rtn.parent = rtn.nd;
            rtn.onLeft = cmpVal < 0;
            rtn.nd = rtn.onLeft ? rtn.nd.left : rtn.nd.right;
         }
      }
      
      return rtn;
   }
   
   @Override
   public V get(Object key) {
      FindResult res = findNode(root, key);
      return res.nd == null ? null : (V)res.nd.data;
   }
   
   @Override
   public V put(K key, V data) {
      FindResult res = findNode(root, key);
      V rtn = null;
      
      if (res.nd == null) {
         if (size == 0)
            root = new Node(key, data, null, null);
         else if (res.onLeft)
            res.parent.left = new Node(key, data, null, null);
         else
            res.parent.right = new Node(key, data, null, null);

         size++;
      }
      else {
         rtn = (V)res.nd.data;
         res.nd.data = data;
      }
      
      return rtn;
   }
   
   @Override
   public boolean containsValue(Object val) {
      return findNodeByValue(root, val) != null;
   }

   private Node findNodeByValue(Node rt, Object val) {
      Node rtn = null;

      if (rt != null)
         if (rt.data.equals(val))
            rtn = rt;
         else if ((null == (rtn = findNodeByValue(rt.left, val))))
            rtn = findNodeByValue(rt.right, val);

      return rtn;
   }
   
   @Override
   public V remove(Object key) {
      FindResult res = findNode(root, key);
      Node child, alt, altPar;
      Object rtn = null;
      boolean altOnLeft = true;
      
      if (res.nd != null) {
         rtn = res.nd.data;
         
         if (res.nd.left == null || res.nd.right == null) {
            child = res.nd.left == null ? res.nd.right : res.nd.left;
            if (res.parent == null)
               root = child;
            else if (res.onLeft)
               res.parent.left = child;
            else
               res.parent.right = child;
         }
         else {
            for (altPar = res.nd, alt = altPar.left; alt.right != null;) {
               altPar = alt;
               alt = altPar.right;
            }

            if (altPar == res.nd)
               altPar.left = alt.left;
            else
               altPar.right = alt.left;

            res.nd.data = alt.data;
            res.nd.key = alt.key;
         }
         size--;
      }
      return (V)rtn;
   }
   
   @Override
   public void putAll(Map<? extends K, ? extends V> mp) {
      for (Map.Entry<? extends K, ? extends V> ent: mp.entrySet())
         put(ent.getKey(), ent.getValue());
   }

   @Override
   public String toString() {
      StringWriter rtn = new StringWriter();
      PrintWriter out = new PrintWriter(rtn);
      
      printTree(root, out, 0);
      
      return rtn.toString();
   }
   
   private void printTree(Node root, PrintWriter out, int indent) {
      int ndx;
       
      if (root != null) {
         printTree(root.left, out, indent+1);
         for (ndx = 0; ndx < indent; ndx++)
            out.print(indentStr);
         out.printf("Key: %s  Value %s\n", root.key.toString(),
          root.data.toString());
         printTree(root.right, out, indent+1);
      }
   }
   
   public double getAvePathLen() {
      return calcPathLen(root, 0) / (double) size;
   }
   
   private int calcPathLen(Node root, int depth) {
      return root == null ? 0 : 1 + calcPathLen(root.left, depth + 1) +
       calcPathLen(root.right, depth + 1);
   }
   
   private class TreeIterator implements Iterator<Node> {
      private Deque<Node> stack;

      public TreeIterator() {
         stack = new LinkedList<Node>();
         if (root != null) {
            stack.push(root);
            while (stack.getFirst().left != null)
               stack.push(stack.getFirst().left);
         }
      }

      @Override
      public boolean hasNext() {
         return !stack.isEmpty();
      }

      @Override
      public Node next() {
         Node rtn = stack.pop();
         
         if (rtn.right != null) {
            stack.push(rtn.right);
            while (stack.getFirst().left != null)
               stack.push(stack.getFirst().left);
         }

         return rtn;
      }

      @Override
      public void remove() {
         Node nd = null;
         FindResult res = findNode(root, stack.getFirst().key);

         if (res.nd.left != null)
            for (nd = res.nd.left; nd.right != null; nd = nd.right)
               ;

         else if (!res.onLeft) nd = res.parent;

         else {
            for (res = findNode(root, res.parent.key); res.onLeft;
             res = findNode(root, res.parent.key))
               ;
            nd = res.parent;
         }

         TreeMap.this.remove(nd.key);
      }
   }
   
   private class KeySet implements Set<K> {
      @Override
      public boolean add(K key) {throw new UnsupportedOperationException();}

      @Override
      public boolean addAll(Collection<? extends K> c) {
         throw new UnsupportedOperationException();
      }

      @Override
      public boolean contains(Object key) {return containsKey(key);}
      
      @Override
      public void clear() {TreeMap.this.clear();} 

      @Override
      public boolean containsAll(Collection<?> c) {
         for (Object obj: c)
            if (!containsValue(obj))
               return false;
         return true;
      }

      @Override
      public boolean isEmpty() {return root == null;}

      @Override
      public Iterator<K> iterator() {
         return new Iterator<K>() {
            private TreeIterator itr;
            
            {
               itr = new TreeIterator();
            }
            
            @Override
            public boolean hasNext() {return itr.hasNext();}

            @Override
            public K next() {return (K)itr.next().key;}

            @Override
            public void remove() {itr.remove();}
         };
      }

      @Override
      public boolean remove(Object key) {
         return TreeMap.this.remove(key) != null;
      }

      @Override
      public boolean removeAll(Collection<?> keys) {
         boolean changed = false;
         
         for (Object key: keys)
            changed = changed | remove(key);
         
         return changed;
      }

      @Override
      public boolean retainAll(Collection<?> arg0) {
         throw new UnsupportedOperationException();
      }

      @Override
      public int size() {return size;}

      @Override
      public Object[] toArray() {
         Object[] rtn = new Object[size];
         int ndx = 0;
         
         for (K key: this)
            rtn[ndx++] = key;
         return rtn;
      }

      @Override
      public <K> K[] toArray(K[] arr) {
         K[] rtn = arr.length >= size ? arr : (K[])new Object[size];
         int ndx = 0;
         
         for (Object key: this)
            rtn[ndx++] = (K)key;
         return rtn;
      }
   }
   
   @Override
   public Set<K> keySet() {return new KeySet();}
   
   @Override
   public Collection values() {
      return new Collection<V>() {
         public boolean add(V v) {throw new UnsupportedOperationException();}

         public boolean addAll(Collection<? extends V> c) {
            throw new UnsupportedOperationException();
         }

         public void clear() {TreeMap.this.clear();}

         public boolean contains(Object o) {
            return findNodeByValue(root, o) != null;
         }

         public boolean containsAll(Collection<?> c) {
            for (Object obj: c)
               if (findNodeByValue(root, obj) == null)
                  return false;
            return true;
         }

         public boolean equals(Object o) {
            Collection<V> c;
            Iterator<V> it1, it2;

            if (o == null || !(o instanceof Collection))
               return false;

            c = (Collection)o;

            if (size() != c.size())
               return false;

            for (it1 = iterator(), it2 = c.iterator(); it1.hasNext();)
               if (it1.next() != it2.next())
                  return false;

            return true;
         }

         public boolean isEmpty() {return size == 0;}

         public Iterator<V> iterator() {
            return new Iterator<V>() {
               private TreeIterator itr;
            
               {
                  itr = new TreeIterator();
               }
            
               @Override
               public boolean hasNext() {return itr.hasNext();}

               @Override
               public V next() {return (V)itr.next().data;}

               @Override
               public void remove() {itr.remove();}
            };
         }

         public boolean remove(Object o) {
            Node nd;

            return (nd = findNodeByValue(root, o)) == null ? false :
             TreeMap.this.remove(nd.key) != null;
         }

         public boolean removeAll(Collection<?> c) {
            boolean changed = false;
         
            for (Object val: c)
               changed = changed | remove(val);
         
            return changed;
         }

         public boolean retainAll(Collection<?> c) {
            throw new UnsupportedOperationException();
         }

         public int size() {return size;}

         public Object[] toArray() {
            Object[] rtn = new Object[size];
            int ndx = 0;
         
            for (V val: this)
               rtn[ndx++] = val;
            return rtn;
         }

         public <V> V[] toArray(V[] arr) {
            int ndx = 0;

            V[] rtn = arr.length >= size ? arr : (V[])new Object[size];

            for (Object val: this)
               rtn[ndx++] = (V)val;
            return rtn;
         }
      };
   }

   @Override
   public Set<Map.Entry<K, V>> entrySet() {
      return new Set<Map.Entry<K, V>>() {
         public boolean add(Map.Entry<K, V> m) {
            throw new UnsupportedOperationException();
         }

         public boolean addAll(Collection<? extends Map.Entry<K, V>> c) {
            throw new UnsupportedOperationException();
         }

         public void clear() {TreeMap.this.clear();}

         public boolean contains(Object o) {
            return containsKey(((Map.Entry<K, V>)o).getKey());
         }

         public boolean containsAll(Collection<?> c) {
            for (Object obj: c)
               if (!containsValue(obj))
                  return false;
            return true;
         }

         public boolean isEmpty() {return size == 0;}

         public Iterator<Map.Entry<K, V>> iterator() {
            return new Iterator<Map.Entry<K, V>>() {
               private TreeIterator itr;

               {
                  itr = new TreeIterator();
               }

               @Override
               public boolean hasNext() {return itr.hasNext();}

               @Override
               public Map.Entry<K, V> next() {
                  Node nd = itr.next();
                  return new TreeEntry((K)nd.key, (V)nd.data);
               }

               @Override
               public void remove() {itr.remove();}
            };
         }

         public boolean remove(Object o) {
            Node nd = findNode(root, ((Map.Entry)o).getKey()).nd;

            return nd != null && nd.data.equals(((Map.Entry)o).getValue()) ?
             TreeMap.this.remove(((Map.Entry)o).getKey()) != null : false;
         }

         public boolean removeAll(Collection<?> c) {
            boolean changed = false;

            for (Object entry: c)
               changed = changed | remove(entry);

            return changed;
         }

         public boolean retainAll(Collection<?> c) {
            throw new UnsupportedOperationException();
         }

         public int size() {return TreeMap.this.size();}

         public Object[] toArray() {
            Object[] rtn = new Object[size];
            int ndx = 0;

            for (Map.Entry<K, V> entry: this)
               rtn[ndx++] = entry;
            return rtn;
         }

         public <T> T[] toArray(T[] arr) {
            Object[] rtn = arr.length >= size ? arr :
             new Object[size];
            int ndx = 0;

            for (Object entry: this)
               rtn[ndx++] = (T)entry;
            return (T[])rtn;
         }
      };
   }
}
