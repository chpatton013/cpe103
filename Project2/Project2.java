package com.cstaley.algebra;

import com.cstaley.algebra.ExpressionTree;
import com.cstaley.algebra.ExpressionTree.Rule;
import com.cstaley.algebra.ExpressionTree.Mod;

import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;

public class Project2 {
   private class ModificationObj {
      Rule rule;
      List<Mod> mods;
      List<ExpressionTree> clones;

      public ModificationObj(ExpressionTree tr, Rule rl) {
         rule = rl;
         mods = tr.getMods(rl);
         clones = new ArrayList<ExpressionTree>();

         for (Mod md : mods)
            clones.add(tr.modClone(md));
      }
   }

   private class ExpressionObj {
      ExpressionTree tree;
      int size;
      List<ModificationObj> mods = new ArrayList<ModificationObj>();

      public ExpressionObj(String str) {
         tree = ExpressionTree.fromPostExpr(str);
         size = tree.getNumNodes();

         for (Rule rl : rules)
            mods.add(new ModificationObj(tree, rl));
      }
   }

   private static Rule[] rules = {
      new CommuteRule(),
      new DistributionRule(),
      new FoldConstantsRule(),
      new LeftAssocRule(),
      new ReduceRule(),
      new RightAssocRule()
   };

   private void run() {
      Scanner sc = new Scanner(System.in).useDelimiter("\\s*\n\\s*");
      ExpressionObj expression;
      List<ExpressionObj> expressions = new ArrayList<ExpressionObj>();
      String str;
      int ndx;

      while (sc.hasNext()) {
         expression = new ExpressionObj(sc.next());

         System.out.println();
         System.out.println("For expression: " + expression.tree);

         for (ModificationObj md : expression.mods) {
            System.out.println("Rule " + md.rule + " gives:");
            for (ExpressionTree tr : md.clones)
               System.out.println(tr);
         }
      }
   }

   public static void main(String[] args) {
      Project2 app = new Project2();
      app.run();
   }
}
