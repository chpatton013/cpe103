package com.cstaley.algebra;

import com.cstaley.algebra.ExpressionTree.Node;
import com.cstaley.algebra.ExpressionTree.Rule;

public class FoldConstantsRule implements Rule {
   @Override
   public Node apply(Node nd) {
      int ndx;
      Node copy;
      Object[] ops = new Object[nd.children.length];

      for (ndx = 0; ndx < ops.length; ndx++)
         ops[ndx] = nd.children[ndx].data;

      copy = new Node(Node.Type.CONSTANT, ((Op)nd.data).doOp(ops));

      return copy;
   }

   @Override
   public boolean canApply(Node nd) {
      if (nd.type != Node.Type.OPERATOR ||
       !Op.ofFoldable().contains((Op)nd.data))
         return false;

      for (Node n : nd.children)
         if (n.type != Node.Type.CONSTANT)
            return false;

      return true;
   }

   @Override
   public String toString() {return "Fold Constants";}
}
