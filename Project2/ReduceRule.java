package com.cstaley.algebra;

import com.cstaley.algebra.ExpressionTree.Node;
import com.cstaley.algebra.ExpressionTree.Rule;

public class ReduceRule implements Rule {
   @Override
   public Node apply(Node nd) {
      if (nd.data == Op.PLUS || nd.data == Op.MULTIPLY) {
         return ExpressionTree.copyNode(nd.children[1]);
      }

      else if (nd.data == Op.MINUS)
         return new Node(Node.Type.CONSTANT, new Double(0.0));

      else if (nd.data == Op.DIVIDE)
         return new Node(Node.Type.CONSTANT, new Double(1.0));

      else return null;
   }

   @Override
   public boolean canApply(Node nd) {
      if (nd.type != Node.Type.OPERATOR ||
       !Op.ofReduceable().contains((Op)nd.data))
         return false;

      return
       ((Op)nd.data == Op.PLUS && nd.children[0].data.equals(0.0)) ||
       ((Op)nd.data == Op.MINUS &&
        Node.recEquals(nd.children[0], nd.children[1])) ||
       ((Op)nd.data == Op.MULTIPLY && nd.children[0].data.equals(1.0)) ||
       ((Op)nd.data == Op.DIVIDE &&
        Node.recEquals(nd.children[0], nd.children[1]));
   }

   @Override
   public String toString() {return "Reduce";}
}
