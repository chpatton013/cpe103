package com.cstaley.algebra;

import java.util.Set;
import java.util.EnumSet;
import java.util.Map;
import java.util.HashMap;

public enum Op {
   PLUS {
      public int numOps() {return plusOps;}
      public String toString() {return "+";}
      public Op getComplement() {return MINUS;}
      public Double doOp(Object[] ops) {return (Double)ops[0] + (Double)ops[1];}
   },
   MINUS {
      public int numOps() {return minusOps;}
      public String toString() {return "-";}
      public Op getComplement() {return PLUS;}
      public Double doOp(Object[] ops) {return (Double)ops[0] - (Double)ops[1];}
   },
   MULTIPLY {
      public int numOps() {return multiplyOps;}
      public String toString() {return "*";}
      public Op getComplement() {return DIVIDE;}
      public Double doOp(Object[] ops) {return (Double)ops[0] * (Double)ops[1];}
   },
   DIVIDE {
      public int numOps() {return divideOps;}
      public String toString() {return "/";}
      public Op getComplement() {return MULTIPLY;}
      public Double doOp(Object[] ops) {return (Double)ops[0] / (Double)ops[1];}
   };

   public static int plusOps = 2;
   public static int minusOps = 2;
   public static int multiplyOps = 2;
   public static int divideOps = 2;

   private static Map<String, Op> opMap = new HashMap<String, Op>();
   static {
      for (Op op : values())
         opMap.put(op.toString(), op);
   }

   public static boolean isOperator(String str) {
      return getOperator(str) != null;
   }

   public static Op getOperator(String str) {return opMap.get(str);}

   public static Set<Op> ofCommutable() {
      return EnumSet.of(Op.PLUS, Op.MULTIPLY);
   }

   public static Set<Op> ofAssociable() {
      return EnumSet.of(Op.PLUS, Op.MINUS, Op.MULTIPLY, Op.DIVIDE);
   }

   public static Set<Op> ofDistributable() {
      return EnumSet.of(Op.PLUS, Op.MINUS, Op.MULTIPLY, Op.DIVIDE);
   }

   public static Set<Op> ofReduceable() {
      return EnumSet.of(Op.PLUS, Op.MINUS, Op.MULTIPLY, Op.DIVIDE);
   }

   public static Set<Op> ofFoldable() {
      return EnumSet.of(Op.PLUS, Op.MINUS, Op.MULTIPLY, Op.DIVIDE);
   }

   public static Set<Op> ofAdditive() {
      return EnumSet.of(Op.PLUS, Op.MINUS);
   }

   public static Set<Op> ofMultiplicative() {
      return EnumSet.of(Op.MULTIPLY, Op.DIVIDE);
   }

   public abstract int numOps();
   public abstract Op getComplement();
   public abstract Double doOp(Object[] ops);
}
