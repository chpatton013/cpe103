package com.cstaley.algebra;

import com.cstaley.algebra.ExpressionTree;
import com.cstaley.algebra.ExpressionTree.Node;
import com.cstaley.algebra.ExpressionTree.Rule;

public class CommuteRule implements Rule {
   @Override
   public Node apply(Node nd) {
      Node copy = new Node(nd.type, nd.data);

      copy.children[0] = ExpressionTree.copyNode(nd.children[1]);
      copy.children[1] = ExpressionTree.copyNode(nd.children[0]);

      return copy;
   }

   @Override
   public boolean canApply(Node nd) {
      return nd.type == Node.Type.OPERATOR &&
       Op.ofCommutable().contains((Op)nd.data);
   }

   @Override
   public String toString() {return "Commute";}
}
