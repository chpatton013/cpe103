package com.cstaley.algebra;

import com.cstaley.algebra.ExpressionTree.Node;
import com.cstaley.algebra.ExpressionTree.Rule;

public class RightAssocRule implements Rule {
   @Override
   public Node apply(Node nd) {
      Node copy;

      if (!Op.ofCommutable().contains((Op)nd.data) &&
       !Op.ofCommutable().contains((Op)nd.children[0].data)) {
         copy = new Node(nd.type, nd.data);
         copy.children[1] = new Node(nd.type, ((Op)nd.data).getComplement());
      }
      else if (!Op.ofCommutable().contains((Op)nd.data) &&
       Op.ofCommutable().contains((Op)nd.children[0].data)) {
         copy = new Node(nd.type, ((Op)nd.data).getComplement());
         copy.children[1] = new Node(nd.type, nd.data);
      }
      else if (Op.ofCommutable().contains((Op)nd.data) &&
       !Op.ofCommutable().contains((Op)nd.children[0].data)) {
         copy = new Node(nd.type, ((Op)nd.data).getComplement());
         copy.children[1] = new Node(nd.type, ((Op)nd.data).getComplement());
      }
      else {
         copy = new Node(nd.type, nd.data);
         copy.children[1] = new Node(nd.type, nd.data);
      }

      copy.children[0] =
       ExpressionTree.copyNode(nd.children[0].children[0]);
      copy.children[1].children[0] =
       ExpressionTree.copyNode(nd.children[0].children[1]);
      copy.children[1].children[1] =
       ExpressionTree.copyNode(nd.children[1]);

      return copy;
   }

   @Override
   public boolean canApply(Node nd) {
      return nd.type == Node.Type.OPERATOR &&
       nd.children[0].type == Node.Type.OPERATOR &&
       Op.ofAssociable().contains((Op)nd.data) &&
       Op.ofAssociable().contains((Op)nd.children[0].data) &&
       (Op.ofAdditive().contains((Op)nd.data) &&
       Op.ofAdditive().contains((Op)nd.children[0].data) ||
       Op.ofMultiplicative().contains((Op)nd.data) &&
       Op.ofMultiplicative().contains((Op)nd.children[0].data));
   }

   @Override
   public String toString() {return "Right Assoc";}
}
