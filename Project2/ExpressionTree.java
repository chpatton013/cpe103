package com.cstaley.algebra;

import com.cstaley.algebra.ExpressionTree.Node.Type;

import java.util.List;
import java.util.Iterator;
import java.util.Deque;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Scanner;

public class ExpressionTree implements Cloneable, Iterable {
   public interface Rule {
      public boolean canApply(Node nd);
      public Node apply(Node nd);
   }

   protected static class Node {
      public enum Type {
         HEAD {
            public int numChildren(Object d) {return headChildren;}
            public String toString(Object d) {return null;}
         },
         CONSTANT {
            public int numChildren(Object d) {return constantChildren;}
            public String toString(Object d) {return d.toString();}
         },
         VARIABLE {
            public int numChildren(Object d) {return variableChildren;}
            public String toString(Object d) {return d.toString();}
         },
         OPERATOR {
            public int numChildren(Object d) {return ((Op)d).numOps();}
            public String toString(Object d) {return ((Op)d).toString();}
         };

         public final static int headChildren = 1;
         public final static int constantChildren = 0;
         public final static int variableChildren = 0;

         public abstract int numChildren(Object d);
         public abstract String toString(Object d);
      }

      protected Type type;
      protected Object data;
      protected Node[] children;

      public Node(Type t, Object d) {
         type = t;
         data = d;
         children = new Node[type.numChildren(d)];
      }

      public Type getType() {return type;}
      public Type setType(Type t) {Type rtn = type; type = t; return rtn;}

      public Object getData() {return data;}
      public Object setData(Object d) {
         Object rtn = data;
         data = d;
         return rtn;
      }

      public Node[] getChildren() {return children;}
      public Node[] setChildren(Node[] ch) {
         Node[] rtn = children;
         children = ch;
         return rtn;
      }

      public String toString() {return type.toString(data);}

      public boolean equals(Object obj) {
         Node o;

         if (obj == null || !(obj instanceof Node))
            return false;

         o = (Node)obj;

         return type == o.type && data.equals(o.data);
      }

      protected static boolean recEquals(Node nd1, Node nd2) {
         int ndx;

         if (nd1.type != nd2.type || !nd1.data.equals(nd2.data))
            return false;

         for (ndx = 0; ndx < nd1.children.length; ndx++)
            if (!recEquals(nd1.children[ndx], nd2.children[ndx]))
               return false;

         return true;
      }
   }

   private class TreeIterator implements Iterator<Node> {
      private class ItObj {
         public Node nd;
         public int ndx = 0;

         public ItObj(Node node) {nd = node;}
      }

      private Deque<ItObj> stack;

      public TreeIterator() {
         stack = new LinkedList<ItObj>();

         stack.push(new ItObj(mHead));
         if (mHead.children[0] != null)
            while (stack.getFirst().nd.children.length != 0)
               stack.push(new ItObj(stack.getFirst().nd.children[0]));
      }

      @Override
      public boolean hasNext() {
         return stack.size() > 1;
      }

      @Override
      public Node next() {
         ItObj rtn = stack.pop(), curr = stack.getFirst();

         if (curr.nd != mHead) {
            curr.ndx++;
            if (curr.ndx < curr.nd.children.length) {

               stack.push(curr = new ItObj(curr.nd.children[curr.ndx]));

               while (curr.nd.children.length > 0)
                  stack.push(curr = new ItObj(curr.nd.children[0]));
            }
         }

         return rtn.nd;
      }

      @Override
      public void remove() {
         throw new UnsupportedOperationException();
      }
   }

   public static class Mod {
      Node node;
      Rule rule;

      public Mod (Node nd, Rule rl) {node = nd; rule = rl;}

      public String toString() {return rule + ": " + node;}
   }


   private final static String indStr = "   ";

   private Node mHead = new Node(Type.HEAD, null);

   public static ExpressionTree fromPostExpr(String exp) {
      int ndx;
      Scanner sc = new Scanner(exp);
      Deque<Node> stack = new LinkedList<Node>();
      String temp;
      Node nd;
      ExpressionTree tree = new ExpressionTree();

      while(sc.hasNext()) {
         if (sc.hasNextDouble())
            stack.push(new Node(Type.CONSTANT, sc.nextDouble()));

         else if (Op.isOperator(temp = sc.next())) {
            nd = new Node(Type.OPERATOR, Op.getOperator(temp));
            for (ndx = nd.children.length - 1; ndx >= 0; ndx--)
               nd.children[ndx] = stack.pop();
            stack.push(nd);
         }

         else
            stack.push(new Node(Type.VARIABLE, temp));
      }

      if (stack.size() > 0)
         tree.mHead.children[0] = stack.pop();

      return tree;
   }

   public int getNumNodes() {
      int ndx;
      Iterator<Node> it = iterator();

      for (ndx = 0; it.hasNext(); it.next(), ndx++)
         ;

      return ndx;
   }

   public String getPostExpr() {
      String str = "";

      Iterator<Node> it = iterator();

      while (it.hasNext())
         str += it.next() + " ";

      return str;
   }

   public List<Mod> getMods(Rule vst) {
      List<Mod> list = new ArrayList<Mod>();

      if (mHead.children.length > 0)
         getMods(list, vst, mHead.children[0]);

      return list;
   }

   private void getMods(List<Mod> list, Rule rl, Node nd) {
      int ndx;

      if (rl.canApply(nd))
         list.add(new Mod(nd, rl));

      for (ndx = 0; ndx < nd.children.length; ndx++)
         getMods(list, rl, nd.children[ndx]);
   }

   @Override
   public Object clone() {
      ExpressionTree copy = null;

      try {
         copy = (ExpressionTree)super.clone();
         copy.mHead = new Node(mHead.type, mHead.data);
         if (mHead.children[0] != null)
            copy.mHead.children[0] = copyNode(mHead.children[0]);
      }
      catch (CloneNotSupportedException err) {}

      return copy;
   }

   protected static Node copyNode(Node root) {
      int ndx;
      Node nd = new Node(root.type, root.data);
  
      for (ndx = 0; ndx < nd.children.length; ndx++)
         nd.children[ndx] = copyNode(root.children[ndx]);
  
      return nd;
   }

   public ExpressionTree modClone(Mod md) {
      ExpressionTree copy = null;

      try {
         copy = (ExpressionTree)super.clone();
         copy.mHead = new Node(mHead.type, mHead.data);
         if (mHead.children[0] != null)
            copy.mHead.children[0] = copyNode(mHead.children[0], md);
      }
      catch (CloneNotSupportedException err) {}

      return copy;
   }

   protected static Node copyNode(Node root, Mod md) {
      int ndx;
      Node nd;

      if (md.node == root)
         return md.rule.apply(md.node);
      else {
         nd = new Node(root.type, root.data);
         for (ndx = 0; ndx < nd.children.length; ndx++)
            nd.children[ndx] = copyNode(root.children[ndx], md);
  
         return nd;
      }
   }

   @Override
   public String toString() {
      return getPostExpr();
   }

   @Override
   public boolean equals(Object obj) {
      ExpressionTree o;

      if (obj == null || !(obj instanceof ExpressionTree))
         return false;

      o = (ExpressionTree)obj;

      Iterator<Node> it1 = iterator(), it2 = o.iterator();

      while (it1.hasNext() && it2.hasNext())
         if (!it1.next().equals(it2.next()))
            return false;

      return it1.hasNext() == it2.hasNext();
   }

   public Iterator<Node> iterator() {
      return new TreeIterator();
   }

   public void printTree(ExpressionTree tree) {
      System.out.println("tree:");

      if (tree.mHead.children.length > 0)
         printSubTree(tree.mHead.children[0], 1);

      System.out.println();
   }

   private void printSubTree(Node root, int indent) {
      int ndx1, ndx2;

      System.out.println(root.data);

      for (ndx1 = 0; ndx1 < root.children.length; ndx1++)  {
         for (ndx2 = 0; ndx2 < indent; ndx2++)
            System.out.print(indStr);

         printSubTree(root.children[ndx1], indent + 1);
      }
   }
}

