package com.cstaley.algebra;

import com.cstaley.algebra.Problem;
import com.cstaley.algebra.Problem.Step;
import com.cstaley.algebra.Problem.State;
import com.cstaley.algebra.Solver;
import com.cstaley.algebra.Solver.Solution;
import java.util.Scanner;

public class Project3 {
   public static void main(String[] args) {
      int ndx = 0, maxCost, maxSlns;
      String probStr, solvStr;
      Class probClass, solvClass;
      Problem problem;
      Solver solver;
      Solution[] solutions;
      Scanner sc = new Scanner(System.in);

      System.out.print("Enter problem type, solution type, max cost " +
       "and max # of solutions: ");
      try {
         probStr = sc.next();
         probClass = Class.forName(probStr);
         problem = (Problem)probClass.newInstance();

         solvStr = sc.next();
         solvClass = Class.forName(solvStr);
         solver = (Solver)solvClass.newInstance();

         maxCost = sc.nextInt();
         if (maxCost < 0) throw new Exception() {};

         maxSlns = sc.nextInt();
         if (maxCost < 0) throw new Exception() {};
      }
      catch (Exception err) {System.out.println("Read error: " + err); return;}

      System.out.println("Enter puzzle problem: ");
      try {problem.read(sc);}
      catch (Exception err) {System.out.println("Read Error: " + err); return;}

      solutions = solver.solveProblem(problem, maxCost, maxSlns);
      if (solutions.length == 0)
         System.out.println("No solution exists.");
      else {
         System.out.println("Answers are:");
         for (Solution sln: solutions)
            System.out.println("Answer " + ndx++ + " with " + sln);
      }
   }
}
