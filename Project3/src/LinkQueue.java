package com.cstaley.algebra;

import com.cstaley.algebra.Queue;

public class LinkQueue<Item> implements Queue<Item> {
   protected static class Node {
      public Object data;
      public Node next;
      
      public Node(Object d, Node n) {data = d; next = n;}
   }
   
   protected Node mHead = new Node(null, null), mTail = mHead;

   public Item getFront() {
      return mHead.next == null ? null : (Item)mHead.next.data;
   }

   public boolean isEmpty() {return mHead.next == null;}

   public void add(Item val) {
         mTail.next = new Node(val, null);
         mTail = mTail.next;
   }

   public void remove() {if (mHead.next != null) mHead = mHead.next;}

   public boolean equals(Object obj) {
      Node nd1, nd2;
      LinkQueue o;

      if (obj == null || !(obj instanceof LinkQueue))
         return false;

      o = (LinkQueue)obj;

      for (nd1 = mHead.next, nd2 = o.mHead.next; nd1 != null && nd2 != null;
       nd1 = nd1.next, nd2 = nd2.next) {
         if (!nd1.data.equals(nd2.data))
            return false;
      }

      return nd1 == nd2;
   }

   public Object clone() {
      LinkQueue copy = null;
      Node nd1, nd2;

      try {
         copy = (LinkQueue)super.clone();

         copy.mHead = new Node(null, null);

         for (nd1 = mHead.next, nd2 = copy.mHead; nd1 != null;
          nd1 = nd1.next, nd2 = nd2.next)
            nd2.next = new Node(nd1.data, nd1.next);

         copy.mTail = nd2;
      }
      catch (CloneNotSupportedException err) {}

      return copy;
   }
}

