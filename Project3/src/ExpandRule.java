package com.cstaley.algebra;

import com.cstaley.algebra.ExpressionTree.Node;
import com.cstaley.algebra.ExpressionTree.Rule;

public class ExpandRule implements Rule {
   @Override
   public Node apply(Node nd) {
      boolean root = nd.root;
      Node val = ExpressionTree.copyNode(nd.children[0].children[0]),
       expr1 = ExpressionTree.copyNode(nd.children[0].children[1]),
       expr2 = ExpressionTree.copyNode(nd.children[1]),
       copy = new Node(Node.Type.OPERATOR, Op.MULTIPLY);

      nd.root = false;

      copy.children[0] = val;
      copy.children[1] = new Node(Node.Type.OPERATOR, nd.data);

      copy.children[1].children[0] = expr1;
      copy.children[1].children[1] = new Node(Node.Type.OPERATOR, Op.DIVIDE);

      copy.children[1].children[1].children[0] = expr2;
      copy.children[1].children[1].children[1] = val;

      copy.root = nd.root = root;

      return copy;
   }

   @Override
   public boolean canApply(Node nd) {
      return nd.type == Node.Type.OPERATOR &&
       Op.ofAdditive().contains((Op)nd.data) &&
       nd.children[0].data == Op.MULTIPLY &&
       nd.children[0].children[0].type == Node.Type.CONSTANT &&
       nd.children[0].children[1].type != Node.Type.CONSTANT;
   }

   @Override
   public String toString() {return "Expand";}
}
