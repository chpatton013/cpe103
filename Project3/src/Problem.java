package com.cstaley.algebra;

import java.util.Iterator;
import java.util.Scanner;

//Classes implementing Solver are expected to be default-constructable.
public interface Problem {
   // One step in the problem space -- changes from one State to another.
   // E.g. A Step in a two-D maze problem would be a directional move.
   // Return 1 from getCost if all steps are equally "costly".
   public interface Step {
      public int getCost();
   }
   
   // One state in the "space" of states offered by the problem.
   // E.g. In a two-D maze problem a State would be one 2-D location.
   public interface State extends Iterable<Step> {
      public Iterator<Step> iterator();  // Get all Steps from this State
      public State applyStep(Step step); // Apply a Step to get a new State
   }
      
   public void read(Scanner in) throws Exception; // Read problem from Scanner
   public State getInitialState();   // Get starting state
   public int getCost(State st);     // How far is state from solution?
}
