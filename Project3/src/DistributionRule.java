package com.cstaley.algebra;

import com.cstaley.algebra.ExpressionTree.Node;
import com.cstaley.algebra.ExpressionTree.Rule;

public class DistributionRule implements Rule {
   @Override
   public Node apply(Node nd) {
      boolean root = nd.root;
      Node copy = new Node(Node.Type.OPERATOR, nd.children[0].data);

      nd.root = false;

      if (Op.ofMultiplicative().contains((Op)nd.data)) {
         copy.children[0] = new Node(Node.Type.OPERATOR, nd.data);
         copy.children[1] = new Node(Node.Type.OPERATOR, nd.data);

         copy.children[0].children[0] =
          ExpressionTree.copyNode(nd.children[0].children[0]);
         copy.children[0].children[1] = ExpressionTree.copyNode(nd.children[1]);

         copy.children[1].children[0] =
          ExpressionTree.copyNode(nd.children[0].children[1]);
         copy.children[1].children[1] = ExpressionTree.copyNode(nd.children[1]);
      }
      else {
         copy.children[0] = new Node(Node.Type.OPERATOR, nd.data);
         copy.children[1] = ExpressionTree.copyNode(nd.children[0].children[1]);

         copy.children[0].children[0] =
          ExpressionTree.copyNode(nd.children[0].children[0]);
         copy.children[0].children[1] =
          ExpressionTree.copyNode(nd.children[1].children[0]);
      }

      copy.root = nd.root = root;

      return copy;
   }

   @Override
   public boolean canApply(Node nd) {
      if (nd.type != Node.Type.OPERATOR ||
       !Op.ofDistributable().contains((Op)nd.data))
         return false;

      return
       (Op.ofMultiplicative().contains((Op)nd.data) &&
        nd.children[0].type == Node.Type.OPERATOR &&
        Op.ofAdditive().contains((Op)nd.children[0].data)) ||
       (Op.ofAdditive().contains((Op)nd.data) &&
        nd.children[0].type == Node.Type.OPERATOR &&
        Op.ofMultiplicative().contains((Op)nd.children[0].data) &&
        nd.children[0].data == nd.children[1].data &&
        Node.recEquals(nd.children[0].children[1],
         nd.children[1].children[1]));
   }

   @Override
   public String toString() {return "Distribute";}
}
