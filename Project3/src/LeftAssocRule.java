package com.cstaley.algebra;

import com.cstaley.algebra.ExpressionTree.Node;
import com.cstaley.algebra.ExpressionTree.Rule;

public class LeftAssocRule implements Rule {
   @Override
   public Node apply(Node nd) {
      boolean root = nd.root;
      Node copy;

      nd.root = false;

      if (!Op.ofCommutable().contains((Op)nd.data) &&
       !Op.ofCommutable().contains((Op)nd.children[1].data)) {
         copy = new Node(nd.type, ((Op)nd.data).getComplement());
         copy.children[0] = new Node(nd.type, nd.data);
      }
      else if (!Op.ofCommutable().contains((Op)nd.data) &&
       Op.ofCommutable().contains((Op)nd.children[1].data)) {
         copy = new Node(nd.type, nd.data);
         copy.children[0] = new Node(nd.type, nd.data);
      }
      else if (Op.ofCommutable().contains((Op)nd.data) &&
       !Op.ofCommutable().contains((Op)nd.children[1].data)) {
         copy = new Node(nd.type, ((Op)nd.data).getComplement());
         copy.children[0] = new Node(nd.type, nd.data);
      }
      else {
         copy = new Node(nd.type, nd.data);
         copy.children[0] = new Node(nd.type, nd.data);
      }

      copy.children[0].children[0] =
       ExpressionTree.copyNode(nd.children[0]);
      copy.children[0].children[1] =
       ExpressionTree.copyNode(nd.children[1].children[0]);
      copy.children[1] =
       ExpressionTree.copyNode(nd.children[1].children[1]);

      copy.root = nd.root = root;

      return copy;
   }

   @Override
   public boolean canApply(Node nd) {
      return nd.type == Node.Type.OPERATOR &&
       nd.children[1].type == Node.Type.OPERATOR &&
       Op.ofAssociable().contains((Op)nd.data) &&
       Op.ofAssociable().contains((Op)nd.children[1].data) &&
       (Op.ofAdditive().contains((Op)nd.data) &&
       Op.ofAdditive().contains((Op)nd.children[1].data) ||
       Op.ofMultiplicative().contains((Op)nd.data) &&
       Op.ofMultiplicative().contains((Op)nd.children[1].data));
   }

   @Override
   public String toString() {return "Left Assoc";}
}
