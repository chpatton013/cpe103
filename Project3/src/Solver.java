package com.cstaley.algebra;

import com.cstaley.algebra.Problem.Step;

// Classes implementing Solver are expected be default-constructable.
public interface Solver {
   static public class Solution {
      public int mCost;               // Cost of this solution.
      public Problem.Step[] mSteps;   // steps from initial state to solved
      public Problem.State[] mStates; // states from second to final
      public Problem.State endState;  // Final state of solution
   }

   // Produce at most numSlns Solutions to the specified Problem, but ignore
   // Solutions whose cost exceeds maxCost.
   public Solution[] solveProblem(Problem prb, int maxCost, int numSlns);
}
