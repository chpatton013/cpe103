package com.cstaley.algebra;

import com.cstaley.algebra.LinkQueue;

import java.util.Comparator;

public class LinkPQueue<Item> extends LinkQueue<Item> {
   protected Comparator<Item> mCmp;
   
   public LinkPQueue(Comparator<Item> cmp) {mCmp = cmp;}
   
   public void add(Item val) {
      Node nd1, nd2;

      for (nd1 = mHead, nd2 = mHead.next; nd2 != null &&
       mCmp.compare((Item)nd2.data, val) >= 0; nd1 = nd1.next, nd2 = nd2.next)
         ;

      nd1.next = new Node(val, nd2);
   }
}

