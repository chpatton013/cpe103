package com.cstaley.algebra;

import com.cstaley.algebra.DblLinkQueue;

import java.util.Comparator;

public class DblLinkPQueue<Item> extends DblLinkQueue<Item> {
   protected Comparator<Item> mCmp;
   
   public DblLinkPQueue(Comparator<Item> cmp) {mCmp = cmp;}

   public void add(Item val) {
      Node nd;

      for (nd = mHead; nd.next != null; nd = nd.next)
         if (mCmp.compare((Item)nd.next.data, val) < 0)
            break;

      nd.next = new Node(val, nd, nd.next);
   }
}

