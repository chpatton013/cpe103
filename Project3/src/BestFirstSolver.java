package com.cstaley.algebra;

import com.cstaley.algebra.ArrPQueue;
import com.cstaley.algebra.LinkPQueue;
import com.cstaley.algebra.DblLinkPQueue;
import com.cstaley.algebra.HeapPQueue;

import com.cstaley.algebra.Stack;
import com.cstaley.algebra.Problem;
import com.cstaley.algebra.Problem.State;
import com.cstaley.algebra.BFSSolver;

import java.util.Comparator;

public class BestFirstSolver extends BFSSolver {
   public BestFirstSolver() {
      queue = new HeapPQueue<Stack<StateHistory>>(new
       Comparator<Stack<StateHistory>>() {
         public int compare(Stack<StateHistory> s1, Stack<StateHistory> s2) {
            int c1 = problem.getCost(s1.top().state),
             c2 = problem.getCost(s2.top().state);

            if (c2 > c1) return 1;
            else if (c2 < c1) return -1;
            else return 0;
         }
      });
   }
}
