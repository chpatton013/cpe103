package com.cstaley.algebra;

import com.cstaley.algebra.ExpressionTree.Node;
import com.cstaley.algebra.ExpressionTree.Rule;

public class ReduceRule implements Rule {
   @Override
   public Node apply(Node nd) {
      boolean root = nd.root;
      Node copy = null;

      nd.root = false;

      if (nd.data == Op.PLUS) {
         copy = ExpressionTree.copyNode(nd.children[1]);
      }

      else if (nd.data == Op.MINUS) {
         if (Node.recEquals(nd.children[0], nd.children[1]))
            copy = new Node(Node.Type.CONSTANT, new Double(0.0));
         else if (nd.children[1].data.equals(0.0))
            copy = ExpressionTree.copyNode(nd.children[0]);
      }

      else if (nd.data == Op.MULTIPLY) {
         if (nd.children[1].data.equals(1.0))
            copy = ExpressionTree.copyNode(nd.children[0]);
         else if (nd.children[1].data.equals(0.0))
            copy = new Node(Node.Type.CONSTANT, new Double(0.0));
         else if (root && nd.children[1].type == Node.Type.CONSTANT)
            copy = ExpressionTree.copyNode(nd.children[0]);
      }

      else if (nd.data == Op.DIVIDE) {
         if (Node.recEquals(nd.children[0], nd.children[1]))
            copy = new Node(Node.Type.CONSTANT, new Double(1.0));
         else if (nd.children[1].data.equals(1.0))
            copy = ExpressionTree.copyNode(nd.children[0]);
      }

      copy.root = nd.root = root;

      return copy;
   }

   @Override
   public boolean canApply(Node nd) {
      if (nd.type != Node.Type.OPERATOR ||
       !Op.ofReduceable().contains((Op)nd.data))
         return false;

      return
       ((Op)nd.data == Op.PLUS && nd.children[1].data.equals(0.0)) ||
       ((Op)nd.data == Op.MINUS && (nd.children[1].data.equals(0.0) ||
        Node.recEquals(nd.children[0], nd.children[1]))) ||
       ((Op)nd.data == Op.MULTIPLY && (nd.children[1].data.equals(1.0) ||
        nd.children[1].data.equals(0.0) ||
        (nd.root && nd.children[1].type == Node.Type.CONSTANT))) ||
       ((Op)nd.data == Op.DIVIDE && (nd.children[1].data.equals(1.0) ||
        Node.recEquals(nd.children[0], nd.children[1])));
   }

   @Override
   public String toString() {return "Reduce";}
}
