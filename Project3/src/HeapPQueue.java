package com.cstaley.algebra;

import com.cstaley.algebra.Queue;

import java.util.Comparator;
import java.util.Scanner;

public class HeapPQueue<Item> implements Queue {
   private class HeapEntry {
      public Object data;
      public int order;

      public HeapEntry(Object data, int order) {
         this.data = data;
         this.order = order;
      }

      public boolean equals(Object obj) {
         return data.equals(((HeapEntry)obj).data);
      }

      public String toString() {
         return "(" + data.toString() + ", " + order + ")";
      }
   }

   private class HeapComparator implements Comparator<HeapEntry> {
      public Comparator<Item> cmp;

      public HeapComparator(Comparator<Item> cmp) {this.cmp = cmp;}

      public int compare(HeapEntry h1, HeapEntry h2) {
         int rtn = cmp.compare((Item)h1.data, (Item)h2.data);

         if (rtn == 0)
            rtn = h1.order < h2.order ? 1 : -1;

         return rtn;
      }
   }

   private final static int dfltSize = 63;

   int mTail = 0, order = 0;
   Object[] mArr = new Object[dfltSize];
   Comparator<HeapEntry> cmp;

   public HeapPQueue(Comparator<Item> cmp) {this.cmp = new HeapComparator(cmp);}

   public void add(Object val) {
      Object[] temp;

      if (mTail == mArr.length) {
         temp = new Object[nextLevel(mTail)];
         System.arraycopy(mArr, 0, temp, 0, mArr.length);
         mArr = temp;
      }

      mArr[mTail] = new HeapEntry(val, order++);
      promote(mTail++);
   }

   public Item getFront() {
      return isEmpty() ? null : (Item)((HeapEntry)mArr[0]).data;
   }

   public void remove() {
      if (mTail > 1) {
         mArr[0] = mArr[--mTail];
         demote(0);
      }
      else if (mTail > 0)
         --mTail;
   }

   public boolean isEmpty() {return mTail == 0;}

   public Object clone() {
      int ndx;
      HeapPQueue copy = null;

      try {
         copy = (HeapPQueue)super.clone();

         copy.mArr = new Object[nextLevel(mTail)];

         System.arraycopy(mArr, 0, copy.mArr, 0, size());
      }
      catch (CloneNotSupportedException err1) {}

      return copy;
   }

   public boolean equals(Object obj) {
      int ndx1, ndx2;
      HeapPQueue o;

      if (obj == null || !(obj instanceof HeapPQueue))
         return false;

      o = (HeapPQueue)obj;

      if (mTail != o.mTail)
         return false;

      for (ndx1 = 0, ndx2 = 0; ndx1 < mTail; ndx1++, ndx2++)
         if (!mArr[ndx1].equals(o.mArr[ndx2]))
            return false;

      return true;
   }
   
   public String toString() {
      int ndx;
      String rtn = "[";

      for (ndx = 0; ndx < mTail; ++ndx) {
         rtn += ndx != mTail - 1 ?
          ((HeapEntry)mArr[ndx]).data.toString() + ", " :
          ((HeapEntry)mArr[ndx]).toString();
      }

      return rtn + "]";
   }

   private int size() {return mTail;};

   private static int childIndex(int ndx) {return 2 * ndx + 1;}

   private static int parentIndex(int ndx) {return (ndx - 1) / 2;}

   private static int nextPower(int ndx) {
      return (int)Math.ceil(log(2, ndx + 1)) + 1;
   }

   private static int nextLevel(int ndx) {
      return (int)Math.pow(2, nextPower(ndx)) - 1;
   }

   private static double log(double base, double val) {
      return Math.log(val) / Math.log(base);
   }

   private void promote(int ndx) {
      Object temp;

      if (ndx == 0 || cmp.compare((HeapEntry)mArr[ndx],
       (HeapEntry)mArr[parentIndex(ndx)]) < 0)
         return;

      temp = mArr[ndx];
      mArr[ndx] = mArr[parentIndex(ndx)];
      mArr[parentIndex(ndx)] = temp;

      promote(parentIndex(ndx));
   }

   private void demote(int ndx) {
      int child;
      Object temp;

      if (childIndex(ndx) >= mTail)
         return;

      child = childIndex(ndx);

      if (cmp.compare((HeapEntry)mArr[ndx], (HeapEntry)mArr[child]) > 0 &&
       cmp.compare((HeapEntry)mArr[ndx], (HeapEntry)mArr[child + 1]) > 0)
         return;

      if (cmp.compare((HeapEntry)mArr[child], (HeapEntry)mArr[child + 1]) < 0)
         ++child;

      temp = mArr[ndx];
      mArr[ndx] = mArr[child];
      mArr[child] = temp;

      demote(child);
   }
}

