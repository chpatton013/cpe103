// Stack Interface for Stacks4 using templates
package com.cstaley.algebra;

public interface Stack<Item> extends Cloneable {
	public abstract void push(Item val);
	public abstract Item top();
	public abstract void pop();
	public abstract boolean isOn(Item val);
	public abstract boolean isEmpty();
   public abstract int getCount();
	public abstract Object clone();
}