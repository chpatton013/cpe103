package com.cstaley.algebra;

import com.cstaley.algebra.Stack;

public class LinkStack<Item> implements Stack<Item> {

   protected static class Node {
      Object data;
      Node next;

      public Node(Object d, Node n) {data = d; next = n;}
   };

   protected Node mHead;
   int mSize;

   public LinkStack() {mHead = null; mSize = 0;}

   public void push(Item val) {mHead = new Node(val, mHead); ++mSize;}

   public Object clone() {
      LinkStack copy = null;
      Node temp, end = null;

      try {copy = (LinkStack)super.clone();}
      catch (CloneNotSupportedException err) {}

      return copy;
   }

   public Item top() {return (Item)mHead.data;}

   public void pop() {mHead = mHead.next; --mSize;}

   public boolean isEmpty() {return mSize == 0;}

   public int getCount() {return mSize;}

   public boolean isOn(Item val) {
      Node temp;

      for (temp = mHead; temp != null && !temp.data.equals(val);
       temp = temp.next)
         ;

      return temp != null;
   }

   public void removeAll(Item val) {
      Node nd1, nd2;

      if (mHead == null)
         return;

      while (mHead.next != null && mHead.data.equals(val))
            mHead = mHead.next;

      if (mHead.data.equals(val)) {
         mHead = null;
         return;
      }

      nd1 = new Node(null, mHead);

      while (nd1.next != null) {
         nd2 = nd1.next;

         if (nd2.data.equals(val))
            nd1.next = nd2.next;
         else
            nd1 = nd1.next;
      }
   }
   
   public String toString() {
      int ndx;
      Node nd = mHead;
      String rtn = "";

      for (ndx = 0; ndx < mSize; ndx++, nd = nd.next)
         rtn += nd.data.toString() + " ";

      return rtn;
   }

   public boolean equals(Object obj) {
      LinkStack rhs;
      Node temp1, temp2;

      if (obj == null || !(obj instanceof LinkStack))
         return false;

      rhs = (LinkStack)obj;

      for (temp1 = mHead, temp2 = rhs.mHead; temp1 != null
       && temp2 != null; temp1 = temp1.next, temp2 = temp2.next)
         if (!temp1.data.equals(temp2.data))
            break;

      return temp1 == null && temp2 == null;
   }   
}

