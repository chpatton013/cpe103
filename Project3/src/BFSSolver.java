package com.cstaley.algebra;

import com.cstaley.algebra.Solver;
import com.cstaley.algebra.Solver.Solution;
import com.cstaley.algebra.Problem;
import com.cstaley.algebra.Problem.Step;
import com.cstaley.algebra.Problem.State;
import com.cstaley.algebra.Queue;
import com.cstaley.algebra.ArrQueue;
import com.cstaley.algebra.LinkQueue;
import com.cstaley.algebra.DblLinkQueue;
import com.cstaley.algebra.Stack;
import com.cstaley.algebra.LinkStack;

import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

import java.util.Date;
import java.text.SimpleDateFormat;

public class BFSSolver implements Solver {
   public static class BFSSolution extends Solution {
      public int mCost;
      public Problem.Step[] mSteps;
      public Problem.State[] mStates;
      public Problem.State endState;

      public BFSSolution(Stack stk) {
         List<Step> stepList = new LinkedList<Step>();
         List<State> stateList = new LinkedList<State>();
         Stack<StateHistory> temp = (Stack)stk.clone();

         mSteps = new Step[0];
         mStates = new State[0];

         endState = temp.top().state;
         mCost = temp.getCount() - 1;

         while (!temp.isEmpty()) {
            if (temp.top().step != null) {
               stepList.add(0, temp.top().step);
               stateList.add(0, temp.top().state);
            }
            temp.pop();
         }

         mSteps = stepList.toArray(mSteps);
         mStates = stateList.toArray(mStates);
      }

      public boolean equals(Object obj) {
         return obj != null && !(obj instanceof BFSSolution) &&
          endState.equals(((BFSSolution)obj).endState);
      }

      public String toString() {
         int ndx;
         String rtn = "";

         rtn += "cost " + mCost + "\n";

         for (ndx = 0; ndx < mSteps.length; ++ndx)
            rtn += "    Do " + mSteps[ndx] + " to get " + mStates[ndx] + "\n";

         rtn += "Final state:\n" + endState.toString() + "\n";

         return rtn;
      }
   }

   protected class StateHistory {
      public State state;
      public Step step;

      public StateHistory(State st) {this(st, null);}
      public StateHistory(State st, Step sp) {state = st; step = sp;}

      public boolean equals(Object obj) {
         if (obj == null || !(obj instanceof StateHistory))
            return false;

         return ((StateHistory)obj).state.equals(state);
      }

      public String toString() {
         return step == null ? " <- " + state.toString() :
          " <- " + state.toString() + " <- " + step.toString();
      }
   }

   Queue<Stack<StateHistory>> queue;
   Problem problem;

   public BFSSolver() {
      queue = new LinkQueue<Stack<StateHistory>>();
   }

   public Solution[] solveProblem(Problem prb, int maxCost, int numSlns) {
      int stateCost, stackCost;
      StateHistory hist;
      Stack<StateHistory> stack, stackClone;
      List<Solution> solutions = new ArrayList<Solution>();
      Solution[] slnArr = new Solution[0];

      problem = prb;

      stack = new LinkStack<StateHistory>();
      stack.push(new StateHistory(prb.getInitialState()));

      stateCost = prb.getCost(stack.top().state);
      stackCost = stack.getCount() - 1;

      if (stateCost == 0)
         solutions.add(new BFSSolution(stack));
      else if (stackCost < maxCost)
         queue.add(stack);

      while (!queue.isEmpty() && solutions.size() < numSlns) {
         stack = queue.getFront();
         stackCost = stack.getCount() - 1;
         queue.remove();

         for (Step sp: stack.top().state) {
            if (stackCost + sp.getCost() <= maxCost) {
               hist = new StateHistory(stack.top().state.applyStep(sp), sp);
               stateCost = prb.getCost(hist.state);

               if (stateCost == 0) {
                  stackClone = (Stack)stack.clone();
                  stackClone.push(hist);
                  solutions.add(new BFSSolution(stackClone));
               }
               else if (stackCost + sp.getCost() < maxCost &&
                !stack.isOn(hist)) {
                  stackClone = (Stack)stack.clone();
                  stackClone.push(hist);
                  queue.add(stackClone);
               }
            }
         }
      }

      return solutions.toArray(slnArr);
   }
}
