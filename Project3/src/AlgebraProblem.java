package com.cstaley.algebra;

import com.cstaley.algebra.Problem;
import com.cstaley.algebra.Problem.Step;
import com.cstaley.algebra.Problem.State;

import com.cstaley.algebra.ExpressionTree;
import com.cstaley.algebra.ExpressionTree.Rule;
import com.cstaley.algebra.ExpressionTree.Mod;

import com.cstaley.algebra.CommuteRule;
import com.cstaley.algebra.DistributionRule;
import com.cstaley.algebra.FoldConstantsRule;
import com.cstaley.algebra.LeftAssocRule;
import com.cstaley.algebra.ReduceRule;
import com.cstaley.algebra.RightAssocRule;

import java.util.Map;
import java.util.TreeMap;

import java.util.List;
import java.util.Iterator;
import java.util.Scanner;

public class AlgebraProblem implements Problem {
   private final static int SOLVED_VAL = 3;
   private static Rule[] rules = {
      new CommuteRule(),
      new LeftAssocRule(),
      new RightAssocRule(),
      new DistributionRule(),
      new ReduceRule(),
      new FoldConstantsRule(),
      new ExpandRule()
   };

   private static Map<String, String> ruleMap = new TreeMap<String, String>();

   {
      String key, value, temp;
      
      temp = rules[0].toString() + ": ";
      value = "Commutation";
      for (Op op: Op.values()) {
         key = temp + op.toString();
         ruleMap.put(key, value);
      }

      temp = rules[1].toString() + ": ";
      value = "Left Association";
      for (Op op: Op.values()) {
         key = temp + op.toString();
         ruleMap.put(key, value);
      }

      temp = rules[2].toString() + ": ";
      value = "Right Association";
      for (Op op: Op.values()) {
         key = temp + op.toString();
         ruleMap.put(key, value);
      }

      temp = rules[3].toString() + ": ";
      value = "Distribution";
      for (Op op: Op.values()) {
         key = temp + op.toString();
         ruleMap.put(key, value);
      }

      temp = rules[4].toString() + ": ";
      value = "Reduction";
      for (Op op: Op.values()) {
         key = temp + op.toString();
         ruleMap.put(key, value);
      }

      temp = rules[5].toString() + ": ";
      value = "Constant Folding";
      for (Op op: Op.values()) {
         key = temp + op.toString();
         ruleMap.put(key, value);
      }

      temp = rules[6].toString() + ": ";
      value = "Expansion";
      for (Op op: Op.values()) {
         key = temp + op.toString();
         ruleMap.put(key, value);
      }
   }

   public class AlgebraStep implements Step {
      Mod mod;

      public AlgebraStep(Mod md) {mod = md;}

      public int getCost() {return 1;}

      public String toString() {return ruleMap.get(mod.toString());}
   }
   
   public class AlgebraState implements State {
      private ExpressionTree tree;

      public AlgebraState(ExpressionTree tr) {tree = tr;}

      public Iterator<Step> iterator() {
         return new Iterator<Step>() {
            int ndx = 0;
            List<Mod> mods;
            Iterator<Mod> modItr;

            {
               mods = AlgebraState.this.tree.getMods(rules[0]);
               modItr = mods.iterator();
            }

            public Step next() {
               if (!modItr.hasNext()) {
                  mods = AlgebraState.this.tree.getMods(rules[++ndx]);
                  modItr = mods.iterator();
               }

               return new AlgebraStep(modItr.next());
            }

            public boolean hasNext() {
               if (modItr.hasNext())
                  return true;

               if (ndx < rules.length - 1) {
                  mods = AlgebraState.this.tree.getMods(rules[++ndx]);
                  modItr = mods.iterator();

                  return hasNext();
               }

               return false;
            }

            public void remove() {throw new UnsupportedOperationException();}
         };
      }

      public State applyStep(Step step) {
         State rtn = new AlgebraState(tree.modClone(((AlgebraStep)step).mod));
         return rtn;
      }

      public boolean equals(Object obj) {
         return obj != null && obj instanceof AlgebraState &&
          tree.equals(((AlgebraState)obj).tree);
      }

      public String toString() {
         if (getCost(this) > 0)
            return tree.toExpression();
         else
            return tree.toEquation();
      }
   }

   private State init;

   public void read(Scanner in) throws Exception {
      int ndx = 0;
      String temp;
      String[] expr = {"", ""};

      while (in.hasNext()) {
         temp = in.next();

         if (temp.equals("="))
            ++ndx;
         else
            expr[ndx] += temp + " ";
      }

      in.close();

      if (!expr[1].equals(""))
         expr[0] += expr[1] + "-";

      init = new AlgebraState(ExpressionTree.fromPostExpr(expr[0]));
   }

   public State getInitialState() {return init;}

   public int getCost(State st) {
      return ((AlgebraState)st).tree.getNumNodes() - SOLVED_VAL;
   }
}

