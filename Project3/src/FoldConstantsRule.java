package com.cstaley.algebra;

import com.cstaley.algebra.ExpressionTree.Node;
import com.cstaley.algebra.ExpressionTree.Rule;

public class FoldConstantsRule implements Rule {
   @Override
   public Node apply(Node nd) {
      int ndx;
      boolean root = nd.root, flag = true;
      Node copy = null;
      Object[] ops;

      nd.root = false;

      for (Node n : nd.children)
         flag = flag && n.type == Node.Type.CONSTANT;

      if (flag) {
         ops = new Object[nd.children.length];
         for (ndx = 0; ndx < ops.length; ndx++)
            ops[ndx] = nd.children[ndx].data;

         copy = new Node(Node.Type.CONSTANT, ((Op)nd.data).doOp(ops));
      }

      else if (nd.data == Op.MINUS && nd.children[0].type !=
       Node.Type.CONSTANT && nd.children[1].type == Node.Type.CONSTANT) {
         copy = ExpressionTree.copyNode(nd);
         copy.data = Op.PLUS;
         copy.children[1].data = new Double(0 - (Double)copy.children[1].data);
      }

      else if (nd.data == Op.DIVIDE &&
       nd.children[0].type != Node.Type.CONSTANT &&
       nd.children[1].type == Node.Type.CONSTANT) {
         copy = ExpressionTree.copyNode(nd);
         copy.data = Op.MULTIPLY;
         copy.children[1].data = new Double(1 / (Double)copy.children[1].data);
      }

      copy.root = nd.root = root;

      return copy;
   }

   @Override
   public boolean canApply(Node nd) {
      boolean flag = true;

      if (nd.type != Node.Type.OPERATOR ||
       !Op.ofFoldable().contains((Op)nd.data))
         return false;

      for (Node n : nd.children)
         flag = flag && n.type == Node.Type.CONSTANT;
      if (flag)
         return true;

      if (nd.data == Op.MINUS && nd.children[0].type != Node.Type.CONSTANT &&
       nd.children[1].type == Node.Type.CONSTANT)
         return true;

      if (nd.data == Op.DIVIDE && nd.children[0].type != Node.Type.CONSTANT &&
       nd.children[1].type == Node.Type.CONSTANT)
         return true;

      return false;
   }

   @Override
   public String toString() {return "Fold Constants";}
}
