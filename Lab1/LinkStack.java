// Stacks3 LinkStack
package com.cstaley.cpe108.Lab1;

public class LinkStack<Item> implements Stack<Item> {

   protected static class Node {
      Object data;
      Node next;

      public Node(Object d, Node n) {data = d; next = n;}
   };

   protected Node mHead;

   public LinkStack() {mHead = null;}

   public void push(Item val) {mHead = new Node(val, mHead);}

   public Object clone() {
      LinkStack copy = null;
      Node temp, end = null;

      try {
         copy = (LinkStack)super.clone();
         for (temp = mHead; temp != null; temp = temp.next) {
            if (end == null)
               end = copy.mHead = new Node(temp.data, null);
            else {
               end.next = new Node(temp.data, null);
               end = end.next;
            }
         }
      } catch (CloneNotSupportedException err) {}

      return copy;
   }

   public Item top() {
      if (mHead == null)
         throw new NullPointerException("Stack underflow");
      return (Item)mHead.data;
   }

   public void pop() {
      if (mHead == null)
         throw new NullPointerException("Stack underflow");
      mHead = mHead.next;
   }

   public boolean isEmpty() {return mHead == null;}

   public boolean isOn(Item val) {
      Node temp;

      for (temp = mHead; temp != null && !temp.data.equals(val);
       temp = temp.next)
         ;

      return temp != null;
   }

   public void removeAll(Item val) {
      Node nd1, nd2;

      if (mHead == null)
         return;

      while (mHead.next != null && mHead.data.equals(val))
            mHead = mHead.next;

      if (mHead.data.equals(val)) {
         mHead = null;
         return;
      }

      nd1 = new Node(null, mHead);

      while (nd1.next != null) {
         nd2 = nd1.next;

         if (nd2.data.equals(val))
            nd1.next = nd2.next;
         else
            nd1 = nd1.next;
      }
   }

   public boolean equals(Object obj) {
      LinkStack rhs;
      Node temp1, temp2;

      if (!(obj instanceof LinkStack))
         return false;

      rhs = (LinkStack)obj;

      for (temp1 = mHead, temp2 = rhs.mHead; temp1 != null
       && temp2 != null; temp1 = temp1.next, temp2 = temp2.next)
         if (!temp1.data.equals(temp2.data))
            break;

      return temp1 == null && temp2 == null;
   }   
}
