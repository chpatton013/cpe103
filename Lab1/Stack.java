// Stack Interface for Prog 9
package com.cstaley.cpe108.Lab1;

public interface Stack<Item> extends Cloneable {
	public abstract void push(Item val);
	public abstract Item top();
	public abstract void pop();
	public abstract boolean isOn(Item val);
	public abstract boolean isEmpty();
	public abstract void removeAll(Item val);
	public abstract Object clone();
}
