// Stack3 ArrStack implementation
package com.cstaley.cpe108.Lab1;

public class ArrStack<Item> implements Stack<Item> {
	protected Object[] data;
	protected int count;
	protected final int sizeIncr = 10;

	public ArrStack() {
		count = 0;
		data = new Object[sizeIncr];
	}

	public Object clone() {
		ArrStack copy = null;

		try {
			copy = (ArrStack)super.clone();
			copy.data = new Object[count];
			System.arraycopy(data, 0, copy.data, 0, count);
		}
		catch (CloneNotSupportedException err) {}

		return copy;
	}

	public void push(Item val) {
		Object[] temp;

		if (count == data.length) {
			temp = new Object[count + sizeIncr];
			System.arraycopy(data, 0, temp, 0, count);
			data = temp;
		}
		data[count++] = val;
	}

	public Item top() {
		if (count == 0)
			throw new IndexOutOfBoundsException("Stack underflow");
		return (Item)data[count-1];
	}

	public void pop() {
		if (count == 0)
			throw new IndexOutOfBoundsException("Stack underflow");
		count--;
	}

	public boolean equals(Object obj) {
		ArrStack stk;

		if (!(obj instanceof ArrStack))
			return false;

		stk = (ArrStack)obj;

		if (count != stk.count)
			return false;

		for (int ndx = 0; ndx < count; ndx++)
			if (!data[ndx].equals(stk.data[ndx])) // Note deep comparison
				return false;

		return true;
	}

	public boolean isOn(Item val) {
		int ndx;

		for (ndx = 0; ndx < count && !data[ndx].equals(val); ndx++)
			;

		return ndx < count;
	}
	
	public boolean isEmpty() {return count == 0;}

   public void removeAll(Item val) {
      int ndx1 = 0, ndx2 = 0, offset = 0;

      for (ndx1 = 0, ndx2 = 0 ; ndx2 < count; ndx2++) {
         if (data[ndx2].equals(val))
            offset++;
         else {
            data[ndx1] = data[ndx2];
            ndx1++;
         }
      }

      count += (ndx1 - ndx2);
   }
}

