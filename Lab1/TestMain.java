package com.cstaley.cpe108.Lab1;

import java.util.Scanner;

public class TestMain
{
	static public void main(String[] args) {
		Stack<Integer> stk1 = new LinkStack<Integer>();
		Stack<Integer> stk2 = new ArrStack<Integer>();
		Scanner input = new Scanner(System.in);
		int count, ndx, val;
			
		System.out.print("How many integers? ");
		count = input.nextInt();
		
		System.out.printf("Enter %d integers: ", count);
		for (ndx = 0; ndx < count; ndx++) {
         val = input.nextInt();		   
			stk1.push(val);
			stk2.push(val);
		}

                System.out.printf("Enter integers to remove, ending with EOF: ");
		while (input.hasNextInt()) {
		   val = input.nextInt();
		   stk2.removeAll(val);
		   stk1.removeAll(val);
		}
		
		System.out.printf("Stack 1: ");
		while (!stk1.isEmpty()) {
		   System.out.printf("%d ", stk1.top());
		   stk1.pop();
		}
		
		System.out.printf("Stack 2: ");
      while (!stk2.isEmpty()) {
         System.out.printf("%d ", stk2.top());
         stk2.pop();
      }
	}
}
