package com.cstaley.cpe103.Hash;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class HashMap<K, V> implements Map<K, V> {
   public class Entry implements Map.Entry<K, V> {
      Object key;
      Object value;

      public Entry(K key, V data) {
         this.key = key;
         this.value = data;
      }

      @Override public K getKey() {return (K)key;}

      @Override public V getValue() {return (V)value;}

      @Override 
      public V setValue(V val) {
         Object rtn = value; 
         
         value = val; 
         return (V)rtn;
      }
   }

   private static final int defSize = 8191;
   private List<Entry>[] table;
   private int size;
   
   public HashMap() {this(defSize);}
   
   public HashMap(int tableSize) {
      table = new LinkedList[tableSize];
   }

   @Override
   public void clear() {
      for (List lst: table)
         if (lst != null)
            lst.clear();
      size = 0;
   }

   @Override
   public boolean isEmpty() {
      return size == 0;
   }
   
   @Override
   public int size() {return size;}
   
   Entry find(List<Entry> lst, Object key) {
      if (lst != null)
         for (Entry ent: lst)
            if (ent.key.equals(key))
               return ent;
      
      return null;
   }

   @Override
   public boolean containsKey(Object key) {
      return find(table[key.hashCode()%table.length], key) != null;
   }

   @Override
   public V get(Object key) {
      Entry ent = find(table[key.hashCode()%table.length], key);
      
      return ent == null ? null : (V)ent.value;
   }
   
   @Override
   public V put(K key, V data) {
      List<Entry> list = table[key.hashCode()%table.length];
      Entry res = find(list, key);
      V rtn = null;
      
      if (res == null) {
         if (list == null)
            list = table[key.hashCode()%table.length] = new LinkedList<Entry>();   
         list.add(new Entry(key, data));
         size++;
      }
      else {
         rtn = (V)res.value;
         res.value = data;
      }
      return rtn;
   }
   
   @Override
   public boolean containsValue(Object val) {
      Iterator<Entry> it = new TblIterator();

      while (it.hasNext())
         if (it.next().getValue().equals(val))
            return true;

      return false;
   }
   
   @Override
   public V remove(Object key) {
      List<Entry> list = table[key.hashCode()%table.length];
      Entry rtn = find(list, (K)key);

      size--;

      return list != null && list.remove(rtn) ? rtn.getValue() : null;
   }
   
   @Override
   public void putAll(Map<? extends K, ? extends V> mp) {
      for (Map.Entry<? extends K, ? extends V> ent: mp.entrySet())
         put(ent.getKey(), ent.getValue());
   }

   @Override
   public String toString() {
      StringWriter rtn = new StringWriter();
      PrintWriter out = new PrintWriter(rtn);
      int ndx = 0;
      
      for (List<Entry> list: table) {
         if (list != null) {
            out.printf("Bucket %d: ", ndx);
            for (Entry ent: list)
               out.printf("(%s, %s, args) ", ent.key, ent.value);
         }

         ndx++;
      }
      
      return rtn.toString();
   }
   
   private class TblIterator implements Iterator<Entry> {
      int ndx;
      List<Entry> list, lastList;
      Iterator<Entry> listIt, lastIt;

      Entry lastEntry;

      public TblIterator() {
         for (ndx = 0; ndx < table.length; ndx++) {
            if (table[ndx] != null && table[ndx].size() > 0) {
               list = table[ndx];
               listIt = list.iterator();
               break;
            }
         }
      }

      public Entry next() {
         lastEntry = listIt.next();

         if (!listIt.hasNext()) {
            for (ndx++; ndx < table.length; ndx++) {
               if (table[ndx] != null && table[ndx].size() > 0) {
                  lastList = list;
                  list = table[ndx];
                  listIt = list.iterator();
                  break;
               }
            }
         }

         return lastEntry;
      }

      public boolean hasNext() {
         return listIt != null && listIt.hasNext();
      }

      public void remove() {
         if (listIt == null)
            throw new IllegalStateException();

         try {
            listIt.remove();
         }
         catch (IllegalStateException err) {
            if (lastList == null)
               throw new IllegalStateException();

            lastList.remove(lastEntry);
         }
      }
   }

   public Iterator<Entry> iterator() {return new TblIterator();}
   
   private class KeySet implements Set<K> {
      @Override
      public boolean add(K key) {throw new UnsupportedOperationException();}

      @Override
      public boolean addAll(Collection<? extends K> c) 
       {throw new UnsupportedOperationException();}

      @Override
      public boolean contains(Object key) {return containsKey(key);}
      
      @Override
      public void clear() {HashMap.this.clear();} 

      @Override
      public boolean containsAll(Collection<?> c) {
         for (Object obj: c)
            if (!containsKey(c))
               return false;
         return true;
      }

      @Override
      public boolean isEmpty() {return size == 0;}

      @Override
      public Iterator<K> iterator() {
         return new Iterator<K>() {
            private Iterator<Entry> it = HashMap.this.iterator();
            
            @Override
            public boolean hasNext() {return it.hasNext();}

            @Override
            public K next() {return (K)it.next().getKey();}

            @Override
            public void remove() {it.remove();}
         };
      }

      @Override
      public boolean remove(Object key) {
         return null != HashMap.this.remove(key);
      }

      @Override
      public boolean removeAll(Collection<?> keys) {
         boolean changed = false;
         
         for (Object key: keys) {
            changed = changed | HashMap.this.remove(key) != null;
         }
         
         return changed;
      }

      @Override
      public boolean retainAll(Collection<?> arg0) {
         throw new UnsupportedOperationException();
      }

      @Override
      public int size() {return size;}

      @Override
      public Object[] toArray() {
         Object[] rtn = new Object[size];
         int ndx = 0;
         
         for (K key: this)
            rtn[ndx++] = key;
         return rtn;
      }

      @Override
      public <K> K[] toArray(K[] arr) {
         K[] rtn = arr.length >= size ? arr : (K[])new Object[size];
         int ndx = 0;
         Class elmType = arr.getClass().getComponentType();
         
         for (Object key: this) {
            if (!elmType.isAssignableFrom(key.getClass()))
               throw new ArrayStoreException();
            rtn[ndx++] = (K)key;
         }
         return rtn;
      }
   } // End class KeySet
   
   @Override
   public Set<K> keySet() {
      return new KeySet();
   }
   
   @Override
   public Collection<V> values() {
      return new Collection<V>() {
         @Override
         public boolean add(V x) 
         {throw new UnsupportedOperationException();}

         @Override
         public boolean addAll(Collection<? extends V> c) 
          {throw new UnsupportedOperationException();}

         @Override
         public boolean contains(Object val) 
          {return containsValue((V) val);}
         
         @Override
         public void clear() {HashMap.this.clear();} 

         @Override
         public boolean containsAll(Collection<?> c) {
            for (Object obj: c)
               if (!contains(c))
                  return false;
            return true;
         }

         @Override
         public boolean isEmpty() {return HashMap.this.isEmpty();}

         @Override
         public Iterator<V> iterator() {
            return new Iterator<V>() {
               private Iterator<Entry> it = HashMap.this.iterator();

               @Override
               public V next() {return (V)it.next().getValue();}

               @Override
               public boolean hasNext() {return it.hasNext();}

               @Override
               public void remove() {it.remove();}
            };
         }

         @Override
         public boolean remove(Object val) {
            Iterator<V> it = iterator();

            while(it.hasNext())
               if (it.next().equals(val)) {
                  it.remove();
                  return true;
               }

            return false;
         }

         @Override
         public boolean removeAll(Collection<?> vals) {
            boolean changed = false;
            
            for (Object val: vals) {
               changed = changed | remove(val);
            }
            
            return changed;
         }

         @Override
         public boolean retainAll(Collection<?> arg0) {
            throw new UnsupportedOperationException();
         }

         @Override
         public int size() {return size;}

         @Override
         public Object[] toArray() {
            Object[] rtn = new Object[size];
            int ndx = 0;
            
            for (V val: this)
               rtn[ndx++] = val;
            return rtn;
         }

         @Override
         public <T> T[] toArray(T[] arr) {
            T[] rtn = arr.length >= size ? arr : (T[])new Object[size];
            int ndx = 0;
            Class elmType = arr.getClass().getComponentType();
            
            for (Object val: this) {
               if (!elmType.isAssignableFrom(val.getClass()))
                  throw new ArrayStoreException();
               rtn[ndx++] = (T)val;
            }
            return rtn;
         }
      };
   }

   @Override
   public Set<Map.Entry<K, V>> entrySet() {
      return new Set<Map.Entry<K, V>> () {

         @Override
         public boolean add(Map.Entry<K, V> x) 
         {throw new UnsupportedOperationException();}

         @Override
         public boolean addAll(Collection<? extends Map.Entry<K, V>> c) 
          {throw new UnsupportedOperationException();}

         @Override
         public boolean contains(Object key) 
          {return containsKey(((Map.Entry<K, V>)key).getKey());}
         
         @Override
         public void clear() {HashMap.this.clear();} 

         @Override
         public boolean containsAll(Collection<?> c) {
            for (Object obj: c)
               if (!contains(c))
                  return false;
            return true;
         }

         @Override
         public boolean isEmpty() {return size == 0;}

         @Override
         public Iterator<Map.Entry<K, V>> iterator() {
            return new Iterator<Map.Entry<K, V>> () {
               private Iterator<Entry> it = HashMap.this.iterator();

               @Override
               public Map.Entry<K, V> next() {return it.next();}

               @Override
               public boolean hasNext() {return it.hasNext();}

               @Override
               public void remove() {it.remove();}
            };
         }

         @Override
         public boolean remove(Object ent) {
            if (find(table[((Entry)ent).key.hashCode() % table.length],
             ((Entry)ent).key).getValue().equals(((Entry)ent).getValue()))
               return HashMap.this.remove(((Entry)ent).key) != null;

            return false;
         }

         @Override
         public boolean removeAll(Collection<?> ents) {
            boolean changed = false;
            
            for (Object ent: ents) {
               changed = changed | remove(ent);
            }
            
            return changed;
         }

         @Override
         public boolean retainAll(Collection<?> arg0) {
            throw new UnsupportedOperationException();
         }

         @Override
         public int size() {return size;}

         @Override
         public Object[] toArray() {
            Object[] rtn = new Object[size];
            int ndx = 0;
            
            for (Map.Entry<K, V> key: this)
               rtn[ndx++] = key;
            return rtn;
         }

         @Override
         public <T> T[] toArray(T[] arr) {
            T[] rtn = arr.length >= size ? arr : (T[])new Object[size];
            int ndx = 0;
            Class elmType = arr.getClass().getComponentType();
            
            
            for (Object entry: this) {
               if (!elmType.isAssignableFrom(entry.getClass()))
                  throw new ArrayStoreException();
               rtn[ndx++] = (T)entry;
            }
            return rtn;
         }
      };
   }
}
