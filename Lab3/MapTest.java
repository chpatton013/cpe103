package com.cstaley.cpe103.Hash;

import java.util.Map;
import java.util.Scanner;

public class MapTest {
   public static void main(String args[]) {
      String command;
      Scanner in = new Scanner(System.in);
      int ndx = 0, ndx2, val;
      Integer objVal;
      String objStr;
      String reply;
      Map<Integer, String>[] map = new Map[2];
      Integer[] intArray;
      String[] strArray;
      Object[] objArray;

      System.out.print("Enter Map type:");
      command = in.next();

      try {
         for (ndx = 0; ndx < map.length; ndx++)
            map[ndx] = (Map<Integer, String>)Class.forName(command).
             newInstance();
      }
      catch (Exception err) {
         System.out.println("Error building Map: " + err);
         return;
      }
     
      do {
         System.out.print("Enter command: ");
         command = in.next().toUpperCase();
         if (!command.equals("Q"))
            ndx = in.nextInt();
         try {
            if (command.equals("P")) {
               val = in.nextInt();
               System.out.printf("Old value: %s\n",
                map[ndx].put(val, in.next()));
            }
            else if (command.equals("S")) {
               System.out.printf("Map %d has size %d\n", ndx,
                map[ndx].size());
            }
            else if (command.equals("E")) {
               reply = map[ndx].isEmpty() ? "empty" : "nonempty";
               System.out.printf("Map %d is %s\n", ndx, reply);
            }
            else if (command.equals("C")) {
               map[ndx].clear();
            }
            else if (command.equals("CK")) {
               objVal = in.nextInt();
               reply = map[ndx].containsKey(objVal) ?
                " contains " : " does not contain ";
               System.out.println("Map " + ndx + reply + objVal);
            }
            else if (command.equals("CV")) {
               objStr = in.next();
               reply = map[ndx].containsValue(objStr) ?
                " contains " : " does not contain ";
               System.out.println("Map " + ndx + reply + objStr);
            }
            else if (command.equals("R")) {
               map[ndx].remove(in.nextInt());
            }
            else if (command.equals("=")) {
               ndx2 = in.nextInt();
               reply = map[ndx].equals(map[ndx2]) ? " does" :
                " doesn't";
               System.out.println("Collection " + ndx + reply +
                "  equals Collection " + ndx2);
            }
            else if (command.equals("PA")) {
               map[ndx].putAll(map[in.nextInt()]);
            }
            else if (command.equals("H")) {
               System.out.printf("Map %d has hashCode %d\n", ndx, 
                map[ndx].hashCode());
            }
            else if (command.equals("KR")) {
               map[ndx].keySet().remove(in.nextInt());
            }
            else if (command.equals("KI")) { 
               System.out.printf("Map %d contains: ", ndx);
               for (int item: map[ndx].keySet())
                  System.out.printf("%d ", item);
               System.out.println();
            }
            else if (command.equals("KA")) {
               objArray = new Object[map[ndx].size()];
               map[ndx].keySet().toArray(objArray);
               System.out.printf("Keys of map %d as array: ", ndx);
               for (Object iVal : objArray)
                  System.out.printf("%d ", iVal);
               System.out.println();
            }
            else if (command.equals("KRA")) {
               map[ndx].keySet().removeAll(map[in.nextInt()].keySet());
            }
            else if (command.equals("VR")) {
               map[ndx].values().remove(in.nextInt());
            }
            else if (command.equals("VI")) {
               System.out.printf("Map %d contains: ", ndx);
               for (String item: map[ndx].values())
                  System.out.printf("%s ", item);
               System.out.println();
            }
            else if (command.equals("VA")) {
               objArray = new Object[map[ndx].size()];
               map[ndx].values().toArray(objArray);
               System.out.printf("Values of map %d as array: ", ndx);
               for (Object sVal : objArray)
                  System.out.printf("%s ", sVal);
               System.out.println();
            }
            else if (command.equals("VRA")) {
               map[ndx].values().removeAll(map[in.nextInt()].values());
            }
            else if (command.equals("ER")) {
               final Integer k = in.nextInt();
               final String v = in.next();

               Map.Entry<Integer, String> entry = new
                Map.Entry<Integer, String> () {
                  private Integer key = k;
                  private String value = v;

                  public boolean equals(Object o) {return false;}

                  public Integer getKey() {return key;}

                  public String getValue() {return value;}

                  public int hashCode() {return key;}

                  public String setValue(String v) {
                     String s = value;
                     value = v;
                     return s;
                  }
               };

               map[ndx].entrySet().remove(entry);
            }
            else if (command.equals("EI")) {
               System.out.printf("Map %d contains: ", ndx);
               for (Map.Entry<Integer, String> entry: map[ndx].entrySet())
                  System.out.printf("(%d, %s) ",
                   entry.getKey(), entry.getValue());
               System.out.println();
            }
            else if (command.equals("EA")) {
               objArray = new Object[map[ndx].size()];
               map[ndx].entrySet().toArray(objArray);
               System.out.printf("Entries of map %d as array: ", ndx);
               for (Object eVal : objArray)
                  System.out.printf("(%d, %s) ",
                   ((Map.Entry<Integer, String>)eVal).getKey(),
                   ((Map.Entry<Integer, String>)eVal).getValue());
               System.out.println();
            }
            else if (command.equals("ERA")) {
               map[ndx].entrySet().removeAll(map[in.nextInt()].entrySet());
            }
            else if (command.equals("D")) {
               System.out.print("\n" + map[ndx]);
            }
            else if (command.equals("Q")) {
               System.out.println("Exiting...");
            }
            else
               System.out.println("Bad command: " + command);
         }
         catch (Exception err) {
            System.out.printf("Error %s\n", err.getMessage());
            err.printStackTrace();
         }
      } while (!command.equals("Q"));
   }
}
